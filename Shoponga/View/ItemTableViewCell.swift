//
//  ItemTableViewCell.swift
//  Shoponga
//
//  Created by Elsammak on 6/23/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//


/*!
    The custom cell for an item listed in the table
*/
import UIKit
//import SWTableViewCell
import MCSwipeTableViewCell

class ItemTableViewCell: MCSwipeTableViewCell {

    
    @IBOutlet weak var itemImageView : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var normalView: UIView!;
    @IBOutlet weak var loadMoreView: UIView!;
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!;
    @IBOutlet weak var leftViewShoppingList: UIView!
    @IBOutlet weak var rightViewShoppingList: UIView!
    
    
    var item: Item!;
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        getLeftViewForSelectionListItems(0);
        self.backgroundColor = UIColor.whiteColor();
        
        itemImageView.layer.borderColor = UIColor.blackColor().CGColor;
        itemImageView.layer.borderWidth = 1.0;
        itemImageView.layer.cornerRadius = 2;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
     /**
     Change the cell to a special "load more" cell
    */
    func switchToLoadingView(){
        
        
        normalView.alpha = 0;
        loadMoreView.alpha = 1;
        loadingWheel.startAnimating();
    }
// MARK: Customizing cell buttons

    /**
    Change the left button of the custom cell depending on the case of the item (added or not)
    
    :param: state State of the item (0 not added, 1 added)
    
    :returns: View to be added behind the cell
    */
    func getLeftViewForSelectionListItems(state: Int) -> UIView{
        
        normalView.alpha = 1;
        loadMoreView.alpha = 0;
        
        
        //set left labels for adding/removing from list. State will be change states only
        var view = UIView(frame: CGRectMake(0, 0, 120, self.frame.size.height));
        var label: UILabel = UILabel(frame: CGRectMake(0, 0, 120, self.frame.size.height));
        
        view.backgroundColor = PINK_COLOR;
        label.backgroundColor = PINK_COLOR;
        
        if(state == 0){
            label.text = "+ Add to list";
        }
        else{
            label.text = "Remove";
        }
        
        label.textColor = UIColor.whiteColor();
        view.addSubview(label);

        return view;
    }
    
    /**
     Get "Add to another List" view
    
     :returns: The new left View behind the cell
    */
    func getLeftViewForShoppingListItem() -> UIView{

        return leftViewShoppingList;
    }

    /**
    Get "Remove" view
    
    :returns: The new right View behind the cell
    */
    
    func getRightViewForShoppingListItem() -> UIView{

        var view = UIView(frame: CGRectMake(0, 0, 80, self.frame.size.height));
        var label: UILabel = UILabel(frame: CGRectMake(0, 0, 80, self.frame.size.height));
        
        view.backgroundColor = PINK_COLOR
        label.backgroundColor = PINK_COLOR;
        

        label.textAlignment = .Right;
        label.text = "Remove ";
        label.textColor = UIColor.whiteColor();
        
        view.addSubview(label);
        
        return view;
    }
    
    
}
