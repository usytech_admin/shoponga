//
//  Logging.swift
//  Shoponga
//
//  Created by Elsammak on 6/17/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation

class Logging {
    
    
    /*!
        Logging data/errors occured
    
        :param: className The class that generates the log message
        :param: description A string describe the looging info
        :param: datatoPrint Th emessage to print, usually the error message
    */
    class func printLog(className : String, description: String, dataToPrint: AnyObject){
        
        println("[\(className)]  Desc: \(description) - \(dataToPrint)");
    }
    
}