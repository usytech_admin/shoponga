//
//  ShoppingListViewController.swift
//  Shoponga
//
//  Created by Elsammak on 6/24/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    ShoppingListViewController is the view controller for showing items for this shopping list
*/
import UIKit
import AHKActionSheet
import EAColourfulProgressView
import MCSwipeTableViewCell

class ShoppingListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate,CreateNewListDelegate,MCSwipeTableViewCellDelegate, UITextFieldDelegate {

    // ShoppingList information view
    @IBOutlet weak var listCostLabel: UILabel!
    @IBOutlet weak var progressView: EAColourfulProgressView!
    
    @IBOutlet weak var budgetAmounttextField: UITextField!
    //IBoutlets
    @IBOutlet weak var itemsTableView : UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    //Var
    private
    var _shoppingList: ShoppingList!
    var _selectedItem: Item!;
    var _indexPathToReload: NSIndexPath!;
    var actionSheet: AHKActionSheet = AHKActionSheet(title: "Select a list");
    
    //Const
    let textCellIdentifier = "ItemCell"
   
    //ViewController
    var itemDetailsViewController: ItemDetailsViewController!;
    var createListViewController : CreateNewListViewController?;
    var sharingViewController : SharingViewController!;
    
    override func viewDidLoad() {
        
        super.viewDidLoad()                
        itemsTableView.registerNib(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: textCellIdentifier)        
        
        
        //Adjusting navigation bar
        self.navigationBar.topItem?.title = _shoppingList.name;
        
        //List selection action sheet
        let buttonFont :UIFont? = UIFont(name: "Arial", size: 12.0);
        actionSheet.buttonTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: buttonFont!]
        actionSheet.cancelButtonTextAttributes = [NSForegroundColorAttributeName: PINK_COLOR, NSFontAttributeName: buttonFont!]
        actionSheet.blurTintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7);
        let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center;
        actionSheet.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: buttonFont!, NSParagraphStyleAttributeName: textStyle];
        actionSheet.cancelButtonNormalBackgroundImage = UIImage(named: "Button-bkg");
        
    }
    
    override func viewWillAppear(animated: Bool) {
    
        updateUpperView();
        self.itemsTableView.reloadData();
    }
    func getCurrentItemsCost() -> Float32{
        
        var totalCost: Float32 = 0;
        for item in _shoppingList.itemsArray{
            totalCost += item.price;
        }
        return totalCost;
    }
    
// MARK: API Methods
    
/**
    This function is to update this shopping list view by previosu items targeted to this list
    
    :param: id The id of the shopping list
*/
    func updateView(id: String){
        _shoppingList = ShoppingListManager.sharedInstance.getShoppingList(id);
    }

    /**
        This function is to update teh upper view information (budget, list cost and progress view)
    */
    
    func updateUpperView(){
    
        //Modifying above view
        var currentCost = getCurrentItemsCost();
        
        budgetAmounttextField.text = _shoppingList.budget.description;
        listCostLabel.text = "List cost: $" + currentCost.description;
        progressView.maximumValue = UInt(_shoppingList.budget);
        
        progressView.updateToCurrentValue(Int(_shoppingList.budget - currentCost), animated: true);
    }
    /**
    Remove Item from Shopping list
    
    :param: item The item to be deleted
    */
    func removeItem(item: Item){
        
        ShoppingListManager.sharedInstance.removeItem(item.listID!, itemToDelete: item);
        item.listID = nil;
        
        self.updateUpperView();
    }
    
    /**
    Add Item to Shopping list
    
    :param: item The item to add
    */
    func addItem(item: Item){
        
        //Clear all added items
        actionSheet.clearItems();
        
        //Add new List button
        actionSheet.cancelButtonTitle = "Create new List"
        actionSheet.cancelHandler = {
            (actionSheet) in
            
            if(self.createListViewController == nil){
                self.createListViewController = CreateNewListViewController(nibName: "CreateNewListViewController", bundle: nil);
            }
            
            
            self.createListViewController!.delegate = self;
            self.providesPresentationContextTransitionStyle = true;
            self.definesPresentationContext = true;
            
            self.createListViewController?.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            
            self.navigationController?.presentViewController(self.createListViewController!, animated: true, completion:{
                () in
                self.createListViewController!.itemToAdd = item;
            })
        }
        
        //Add each ShoppingList to the action sheet
        for index in 0..<ShoppingListManager.sharedInstance.getOrderedShoppingList().count{
            
            actionSheet.addButtonWithTitle(ShoppingListManager.sharedInstance.getOrderedShoppingList()[index].name, image: nil, type: AHKActionSheetButtonType.Default, handler: {
                (actionSheet) in

                var shoppingList = ShoppingListManager.sharedInstance.getOrderedShoppingList()[index];
                if(shoppingList.id == self._shoppingList.id){
                    return;
                }

                shoppingList.itemsArray.append(item);
                RetailerManager.sharedInstance.currentRetailer.retailerObject.addItem(item);
                item.listID = shoppingList.id;
            })
        }
        actionSheet.show();
    }
    
    
// MARK: IBAction Methods
    
    @IBAction func backButtonClicked(){
        
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        
        if segue!.identifier == "ItemDetailsViewController" {
            itemDetailsViewController = segue?.destinationViewController as! ItemDetailsViewController;
            itemDetailsViewController.item = _selectedItem;
        }
        else if segue!.identifier == "SharingViewController"{
            sharingViewController = segue?.destinationViewController as! SharingViewController;
            sharingViewController.shoppingList = _shoppingList;
        }
    }

// MARK: UITextFieldDelegate
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        _shoppingList.budget = textField.text.floatValue;
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.background = UIImage(named: "InputBGSmallActive");
        return true;
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        textField.background = UIImage(named: "InputSmallBG");
        return true;
    }
    
// MARK: UITableViewDlegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        

        return 100;
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _shoppingList.itemsArray.count;

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
            
        var cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier) as? ItemTableViewCell
        
        if cell == nil {
            cell = ItemTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: textCellIdentifier)
        }
        
        
        var item: Item = _shoppingList.itemsArray[indexPath.row];
        if let image = item.smallImage{
            cell?.itemImageView.imageFromUrlAsync(image);
        }
        cell?.nameLabel?.text = item.name;
        cell?.priceLabel?.text = item.getPriceString()
        cell?.delegate = self;
        
        //Customise cell
        
        var leftView = cell?.getLeftViewForShoppingListItem();
        var rightView = cell?.getRightViewForShoppingListItem()
        
        //setup left view (move to another list)
        cell?.setSwipeGestureWithView(leftView, color: GRAY_COLOR, mode: .Switch, state: .State1, completionBlock: {
            (cell: MCSwipeTableViewCell?, state: MCSwipeTableViewCellState, mode: MCSwipeTableViewCellMode) in
            
            self.addItem(item);
        })

        //Setup right view (remove)
        
        cell?.setSwipeGestureWithView(rightView, color: PINK_COLOR, mode: .Exit, state: .State3, completionBlock: {
            (cell: MCSwipeTableViewCell?, state: MCSwipeTableViewCellState, mode: MCSwipeTableViewCellMode) in
            
            self.removeItem(item);
            self.itemsTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade);
            self.itemsTableView.reloadData();
            
        })


        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        _selectedItem = _shoppingList.itemsArray[indexPath.row];
        //Show alert for deletion
        self.performSegueWithIdentifier("ItemDetailsViewController", sender: self);
        
    }
    

//MARK: Create new Shopping list Delegate
    
    func newListCreated(item: Item){
        
        _shoppingList.itemsArray.removeAtIndex(_indexPathToReload.row);
        updateUpperView();
        self.itemsTableView.reloadData();
    }

}
