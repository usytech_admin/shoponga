//
//  CreateNewListViewController.swift
//  Shoponga
//
//  Created by Elsammak on 7/30/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import UIKit

protocol CreateNewListDelegate{
    
    func newListCreated(item: Item);
}
class CreateNewListViewController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var listItemTextField: UITextField!
    @IBOutlet weak var listBudgetTextField: UITextField!
    @IBOutlet weak var dialogView: UIView!
    
    //vars
    var itemToAdd: Item!;
    
    //Delegates
    var delegate:CreateNewListDelegate!;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        listItemTextField.text = "";
        listBudgetTextField.text = "";
        listItemTextField.becomeFirstResponder();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK: IBActoin methods
    
    @IBAction func createListButtonPressed(sender: UIButton) {
        
        if(listItemTextField.text.isEmpty || listBudgetTextField.text.isEmpty){
            return;
        }
        
        //Create new list and add the item
        var shoppingList: ShoppingList = ShoppingListManager.sharedInstance.createNewShoppingList(listItemTextField.text, budget: listBudgetTextField.text.floatValue);
        RetailerManager.sharedInstance.currentRetailer.retailerObject.addItem(itemToAdd);
        shoppingList.itemsArray.append(itemToAdd);
        itemToAdd.listID = shoppingList.id;
        self.dismissViewControllerAnimated(true, completion: nil);
        delegate.newListCreated(itemToAdd);
    }
    

    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    
        for touch: AnyObject in touches {
            let location = touch.locationInView(dialogView)
            
            if(location.x < 0 || location.y < 0 || location.x > dialogView.bounds.width || location.y > dialogView.bounds.height){
                self.dismissViewControllerAnimated(true, completion: nil);
                
            }

        }
    }
}
