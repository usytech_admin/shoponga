//
//  ItemDetailsViewController.swift
//  Shoponga
//
//  Created by Elsammak on 6/23/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    The ItemDetailsViewController class is the view controller responsible for showing product detials and allow adding it
    to an existing or new shopping list
*/

import UIKit
import AHKActionSheet

class ItemDetailsViewController: UIViewController {

    
    //IBoutlets
    @IBOutlet weak var productImage : UIImageView!
    @IBOutlet weak var productName : UILabel!
    @IBOutlet weak var productPrice : UILabel!
    @IBOutlet weak var navigationBar : UINavigationBar!
    @IBOutlet weak var addToListButton: UIButton!
    @IBOutlet weak var productDescription: UIWebView!
    
    //ViewControllers
    var allShoppingListsViewController : AllShoppingListsViewController!;
    var allShoppingNavigationController: UINavigationController!;
    var createListViewController : CreateNewListViewController?;
    
    //Views
    var actionSheet: AHKActionSheet = AHKActionSheet(title: "Select a list");
    
    //Item
    var item: Item = Item();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.topItem?.title = item.name;        
        
        //List selection action sheet
        let buttonFont :UIFont? = UIFont(name: "Arial", size: 12.0);
        actionSheet.buttonTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: buttonFont!]
        actionSheet.cancelButtonTextAttributes = [NSForegroundColorAttributeName: PINK_COLOR, NSFontAttributeName: buttonFont!]
        actionSheet.blurTintColor = UIColor(red: 61/255, green: 73/255, blue: 83/255, alpha: 0.7);
        let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center;
        actionSheet.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: buttonFont!, NSParagraphStyleAttributeName: textStyle];
        actionSheet.cancelButtonNormalBackgroundImage = UIImage(named: "Button-bkg");
    }

    override func viewWillAppear(animated: Bool) {
        refresh();
    }
// MARK: Refresh View
    
    /// Refresh view with the new item's data
    func refresh(){
        
        if let image = item.smallImage{
            productImage.imageFromUrlAsync(image);
        }

        productName.text? = item.name;
        productPrice.text? = item.getPriceString()
        
        if let description = item.desc{
            productDescription.loadHTMLString(description, baseURL: nil);
        }
        
//        if(item.listID != nil){ //Item is not added yet
//            addToListButton.alpha = 0;
//        }
//        else{
//            addToListButton.alpha = 1;
//        }
        
    }
    
    
    /**
    Add Item to Shopping list
    
    :param: item The item to add
    */
    func addItem(){
        
        //Clear all added items
        actionSheet.clearItems();
        
        //Add new List button
        actionSheet.cancelButtonTitle = "Create new List"
        actionSheet.cancelHandler = {
            (actionSheet) in
            
            if(self.createListViewController == nil){
                self.createListViewController = CreateNewListViewController(nibName: "CreateNewListViewController", bundle: nil);
            }
            
            self.createListViewController!.delegate = nil;
            self.providesPresentationContextTransitionStyle = true;
            self.definesPresentationContext = true;
            
            self.createListViewController?.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            
            self.navigationController?.presentViewController(self.createListViewController!, animated: true, completion:{
                () in
                self.createListViewController!.itemToAdd = self.item;
            })
        }
        
        //Add each ShoppingList to the action sheet
        for index in 0..<ShoppingListManager.sharedInstance.getOrderedShoppingList().count{
            
            actionSheet.addButtonWithTitle(ShoppingListManager.sharedInstance.getOrderedShoppingList()[index].name, image: nil, type: AHKActionSheetButtonType.Default, handler: {
                (actionSheet) in
                
                var shoppingList = ShoppingListManager.sharedInstance.getOrderedShoppingList()[index];
                shoppingList.itemsArray.append(self.item);
                RetailerManager.sharedInstance.currentRetailer.retailerObject.addItem(self.item);
                self.item.listID = shoppingList.id;
            })
        }
        actionSheet.show();
    }
// MARK: IBOutlets methods
    
    @IBAction func backButtonClicked(){
        
        navigationController?.popViewControllerAnimated(true);
        
    }

    @IBAction func addToListButtonPressed(sender: UIButton) {
        
        addItem();
    }
}
