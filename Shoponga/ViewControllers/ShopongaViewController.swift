//
//  ShopongaViewController.swift
//  Shoponga
//
//  Created by Elsammak on 6/22/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    This is the main View controller of the app where user
    can search for products and select any item.
*/

import UIKit
import SWTableViewCell
import AHKActionSheet
import PopoverView
import MCSwipeTableViewCell

class ShopongaViewController: UIViewController,UISearchBarDelegate, UISearchControllerDelegate,UITableViewDataSource, UITableViewDelegate,RetailerModelToViewDelegate,SWTableViewCellDelegate,PopoverViewDelegate,MCSwipeTableViewCellDelegate {
    
    let textCellIdentifier = "ItemCell"
    
    //IBoutlets
    @IBOutlet var itemsTableView : UITableView!
    @IBOutlet var loadMoreButton : UIButton!
    @IBOutlet var loadingView : UIView!;
    @IBOutlet var wizardView : UIView!;
    @IBOutlet var menuButton : UIBarButtonItem!
    @IBOutlet var searchButton : UIBarButtonItem!;
    @IBOutlet var categoriesButton : UIBarButtonItem!
    
    
    //View Controllers
    var itemDetailsViewController : ItemDetailsViewController!;
    var allShoppingListsViewController : AllShoppingListsViewController!;    
    var searchViewController: SearchViewController!
    var createListViewController : CreateNewListViewController?;
    
    
    //Views
    var categoriesPopoverMenu: PopoverView!;
    var actionSheet: AHKActionSheet = AHKActionSheet(title: "Select a list");
    
    //Vars
    var searchQuery:String!
    var categoriesArray: [String]!
    
    private
    var _itemsArray : [Item] = [];
    var _indexPathToReload: NSIndexPath!;
    var _selectedItem: Item!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.barTintColor = GRAY_COLOR
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()];
//        self.revealViewController().rearViewRevealWidth = 62
        self.revealViewController().panGestureRecognizer().enabled = false;
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        //Adding right side buttons to navigation bar
        var categoriesImage   : UIImage = UIImage(named: "List")!
        var categoriesButton : UIBarButtonItem = UIBarButtonItem(image: categoriesImage,  style: UIBarButtonItemStyle.Plain, target: self, action: Selector("categoriesButtonPressed:"))
        categoriesButton.tintColor = UIColor.whiteColor();
        var buttons : NSArray = [categoriesButton, searchButton]
        self.navigationItem.rightBarButtonItems = buttons as [AnyObject]
        
        //List selection action sheet
        let buttonFont :UIFont? = UIFont(name: "Arial", size: 12.0);
        actionSheet.buttonTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: buttonFont!]
        actionSheet.cancelButtonTextAttributes = [NSForegroundColorAttributeName: PINK_COLOR, NSFontAttributeName: buttonFont!]
        actionSheet.blurTintColor = UIColor(red: 61/255, green: 73/255, blue: 83/255, alpha: 0.7);
        let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center;
        actionSheet.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: buttonFont!, NSParagraphStyleAttributeName: textStyle];
        actionSheet.cancelButtonNormalBackgroundImage = UIImage(named: "Button-bkg");
        
        //Define UIViewControllers
        itemsTableView.registerNib(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: textCellIdentifier)
        RetailerManager.sharedInstance.currentRetailer.delegate = self;
        self.searchViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SearchViewController") as! SearchViewController;                        
        
    }
    

// MARK: UITableViewDlegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 100;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _itemsArray.count + 1;
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(tableView == itemsTableView && _itemsArray.count > 0){
            
            
            var cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier) as? ItemTableViewCell
            
            if cell == nil {
                cell = ItemTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: textCellIdentifier)
            }
            
            
            //Check for load more cell
            if(indexPath.row >= _itemsArray.count){
                
                cell?.switchToLoadingView();
                RetailerManager.sharedInstance.searchForItem(searchQuery!);
            }
            else{
                var item: Item = _itemsArray[indexPath.row];
                
                var leftView = cell?.getLeftViewForSelectionListItems(0);
                var isItemAdded = false;
                if(self.checkForItem(item)){
                    isItemAdded = true;
                    leftView = cell?.getLeftViewForSelectionListItems(1);
                }
                

                
                //Customize the hidden scroll view
                //setup left view (Add/remove from list)
                cell?.setSwipeGestureWithView(leftView, color: PINK_COLOR, mode: .Switch, state: .State1, completionBlock: {
                    (cell: MCSwipeTableViewCell?, state: MCSwipeTableViewCellState, mode: MCSwipeTableViewCellMode) in
                    
                    self._indexPathToReload = indexPath;
                    
                    if(!isItemAdded){
                        self.addItem(item);
                    }
                    else{
                        self.removeItem(item);
                    }
                    
                })
                
                
                
                cell?.item = item;
                cell?.delegate = self;
                cell?.itemImageView.image = nil;
                if let image = item.smallImage{
                    cell?.itemImageView.imageFromUrlAsync(image);
                }
                cell?.nameLabel?.text = item.name;
                cell?.priceLabel?.text = item.getPriceString()
                
            }
            
            
            return cell!;
        }
        
        let cell:UITableViewCell = UITableViewCell();
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if(tableView == itemsTableView && _itemsArray.count > 0){  //Get item details
            
            
            if(_itemsArray[indexPath.row].isUpdated){
                
                detailsDataReturned(_itemsArray[indexPath.row]);
            }
            else{
                loadingView.alpha = 0.75;
                RetailerManager.sharedInstance.getItemDetails(_itemsArray[indexPath.row]);
                _itemsArray[indexPath.row].isUpdated = true;
            }

            
        }        
    }
    
    
// MARK: Categories Menu Delegate methods methods
    
    func popoverView(popoverView: PopoverView!, didSelectItemAtIndex index: Int) {
        
        println(CategoriesManager.sharedInstance.categoriesList[index].name);
        var category: Category = CategoriesManager.sharedInstance.categoriesList[index];
        loadingView.alpha = 0.75;
        searchQuery = category.keyWords;
        RetailerManager.sharedInstance.searchForItem(category.keyWords);
    }
    
// MARK: IBOutlet methods
    
    @IBAction func wizardButtonClicked(){
        
        UIView.animateWithDuration(1, animations: {
            self.wizardView.alpha = 0
        })
    }
    
    func categoriesButtonPressed(sender: AnyObject){
        
        var x = Double(UIScreen.mainScreen().bounds.size.width);
        var y = Double(self.navigationController!.navigationBar.bounds.height);
        
        PopoverView.showPopoverAtPoint(CGPoint(x: x, y: y), inView: self.view, withStringArray: CategoriesManager.sharedInstance.getListOfCategoriesNames(), delegate: self);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        
        if segue!.identifier == "SearchViewController" {
            searchViewController = segue?.destinationViewController as! SearchViewController;
            searchViewController.setCurrentShopongaViewController(self);
        }
        else if segue!.identifier == "ItemDetailsViewController" {
            itemDetailsViewController = segue?.destinationViewController as! ItemDetailsViewController;
            itemDetailsViewController.item = _selectedItem;
        }
    }
    
// MARK: RetailerModelToViewDelegate
    func searchDataReturned (arr: Array<Item>){
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
        _itemsArray = _itemsArray + arr ;
        itemsTableView.reloadData();
        loadingView.alpha = 0;
    }
    
    
    func detailsDataReturned(item: Item) {
        
        loadingView.alpha = 0;
        item.isUpdated = true;
        _selectedItem = item;
        self.performSegueWithIdentifier("ItemDetailsViewController", sender: self);
    }
    
// MARK: Managing item methods
    
    /**
    Add Item to Shopping list
    
     :param: item The item to add
    */
    func addItem(item: Item){
        
        //Clear all added items
        actionSheet.clearItems();
        
        //Add new List button
        actionSheet.cancelButtonTitle = "Create new List"
        actionSheet.cancelHandler = {
                (actionSheet) in
            
            if(self.createListViewController == nil){
                self.createListViewController = CreateNewListViewController(nibName: "CreateNewListViewController", bundle: nil);
            }
            
            self.createListViewController!.delegate = nil;
            self.providesPresentationContextTransitionStyle = true;
            self.definesPresentationContext = true;
            
            self.createListViewController?.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
            
            self.navigationController?.presentViewController(self.createListViewController!, animated: true, completion:{
             () in
                self.createListViewController!.itemToAdd = item;
            })
        }
        
        //Add each ShoppingList to the action sheet
        for index in 0..<ShoppingListManager.sharedInstance.getOrderedShoppingList().count{
            
            actionSheet.addButtonWithTitle(ShoppingListManager.sharedInstance.getOrderedShoppingList()[index].name, image: nil, type: AHKActionSheetButtonType.Default, handler: {
                (actionSheet) in
                
                var shoppingList = ShoppingListManager.sharedInstance.getOrderedShoppingList()[index];
                shoppingList.itemsArray.append(item);
                RetailerManager.sharedInstance.currentRetailer.retailerObject.addItem(item);
                item.listID = shoppingList.id;
                self._itemsArray[self._indexPathToReload.row] = item;
                self.itemsTableView.reloadRowsAtIndexPaths([self._indexPathToReload], withRowAnimation: .Fade);
            })
        }
        actionSheet.show();
    }
    
    /**
    Remove Item from Shopping list
    
    :param: item The item to be deleted
    */
    func removeItem(item: Item){
        
        ShoppingListManager.sharedInstance.removeItem(item.listID!, itemToDelete: item);
        item.listID = nil;
        self.itemsTableView.reloadRowsAtIndexPaths([self._indexPathToReload], withRowAnimation: UITableViewRowAnimation.None);
    }
    
    /**
    Check if this item was added before
    
    :param: item The item to check for
    
    :returns: true if exixts otherwise no
    */
    func checkForItem(item: Item) -> Bool{
     
        if(item.listID != nil){
            return true;
        }
        return false;
    }
    
}

