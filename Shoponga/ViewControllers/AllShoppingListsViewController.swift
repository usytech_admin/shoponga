//
//  AllShoppingListsViewController.swift
//  Shoponga
//
//  Created by Elsammak on 6/24/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    AllShoppingListsViewController, show collection of all lists user have on the app
*/

import UIKit

class AllShoppingListsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    //Profile View
    
    //IBoutlets
    @IBOutlet weak var shoppingListsTableView: UITableView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var tableHeaderView: UIView!
    
    //ViewCOntrollers
    var shoppingListViewController: ShoppingListViewController!;
    
    let textCellIdentifier = "TextCell"
    var itemToAdded: Item!;
    
    private
    var _shoppingListsArray: [ShoppingList]!;
    var _selectedShoppingListID : String!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shoppingListViewController = ShoppingListViewController(nibName: "ShoppingListViewController", bundle: nil);
    }

    override func viewWillAppear(animated: Bool) {
        
        _shoppingListsArray = ShoppingListManager.sharedInstance.getOrderedShoppingList();
        shoppingListsTableView.reloadData();
    }
    
    
    func couldDeleteList(shoppingListId: String?) -> Bool{
        
        
        if(shoppingListId == ShoppingListManager.sharedInstance.getDefaultShoppingList().id){
            let myAlert = UIAlertView()
            myAlert.message = "Cannot delete Default list";
            myAlert.addButtonWithTitle("Ok")
            myAlert.delegate = self
            myAlert.show()
            
            return false;
        }
        else if(_shoppingListsArray.count == 1){
            let myAlert = UIAlertView()
            myAlert.message = "you must have at least one list";
            myAlert.addButtonWithTitle("Ok")
            myAlert.delegate = self
            myAlert.show()
            
            return false;
            
        }
        return true;
    }
    /**
        Send the item to AllShoppingListsViewController to bypass after that to the selected ShoppingList
    
        :praram: item The item to bypass to the selected list
    */
    func prepareShoppingListWithItem(item: Item){
        
        self.itemToAdded = item;
    }
    
// MARK: IBAction methods

    @IBAction func editButtonClicked(){
        
        self.shoppingListsTableView.setEditing(!self.shoppingListsTableView.editing, animated: true);
        
        if(self.shoppingListsTableView.editing){
            
            editButton.setTitle("Done", forState: .Normal);
        }
        else{
            
            editButton.setTitle("Edit", forState: .Normal);
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        
        if segue!.identifier == "ItemsListViewController" {
            shoppingListViewController = segue?.destinationViewController as! ShoppingListViewController;
            shoppingListViewController.updateView(_selectedShoppingListID);
        }
       
    }
// MARK: UITableViewDlegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44;
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableHeaderView;
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        
        var cell = tableView.cellForRowAtIndexPath(indexPath);
        cell?.contentView.backgroundColor = PINK_COLOR;
    }
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        
        var cell = tableView.cellForRowAtIndexPath(indexPath);
        cell?.contentView.backgroundColor = GRAY_COLOR;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _shoppingListsArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier) as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: textCellIdentifier)
        }
        
        cell?.selectionStyle = .None;
        var shoppingList: ShoppingList = _shoppingListsArray[indexPath.row]
        cell?.textLabel?.text = shoppingList.name;
        cell?.backgroundColor = GRAY_COLOR;
        cell?.textLabel?.textColor = UIColor.whiteColor();
        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        var shoppingList: ShoppingList = _shoppingListsArray[indexPath.row];
        _selectedShoppingListID = shoppingList.id;
        self.performSegueWithIdentifier("ItemsListViewController", sender: self);
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     
        if(indexPath.row > 0){
            return true;
        }
        return false;
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        
        //Change the two 
        ShoppingListManager.sharedInstance.swapListWithId(_shoppingListsArray[sourceIndexPath.row].id, withListWithId: _shoppingListsArray[destinationIndexPath.row].id)
    }

    func tableView(tableView: UITableView, targetIndexPathForMoveFromRowAtIndexPath sourceIndexPath: NSIndexPath, toProposedIndexPath proposedDestinationIndexPath: NSIndexPath) -> NSIndexPath {
        
        if(proposedDestinationIndexPath.row == 0){
            return NSIndexPath(forRow: 1, inSection: 0);
        }
        return proposedDestinationIndexPath;
    }
    // called when a row deletion action is confirmed
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle,forRowAtIndexPath indexPath: NSIndexPath) {
        
            switch editingStyle {
            case .Delete:
                
                var shoppingList: ShoppingList = _shoppingListsArray[indexPath.row];
                
                if(!couldDeleteList(shoppingList.id)){
                    return;
                }
                // remove the deleted list from the model
                ShoppingListManager.sharedInstance.deleteShoppingList(shoppingList.id);
                _shoppingListsArray.removeAtIndex(indexPath.row);
                
                // remove the deleted item from the `UITableView`
                self.shoppingListsTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade);
                
            default:
                return
            }
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
    
}
