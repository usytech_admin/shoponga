//
//  LoginViewController.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import TwitterKit

class LoginViewController: UIViewController,FBSDKAppInviteDialogDelegate, UITextFieldDelegate {
    
    //IBoutlets
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if(Settings.sharedInstance.getUserId() != nil){
            self.performSegueWithIdentifier("MainViewController", sender: self);
        }
    }

//MARK: IBAction methods

    @IBAction func loginButtonPressed(sender: UIButton) {
        
    }
    
    @IBAction func facebookButtonPressed(sender: UIButton) {
        
        facebookLogin();
    }
    
    @IBAction func twitterButtonPressed(sender: UIButton) {
     
        twitterLogin();
    }
//MARK: Facebook APIs
    func facebookLogin(){
        
        if(FBSDKAccessToken.currentAccessToken() == nil){ //User logged in
            
            var loginManager: FBSDKLoginManager = FBSDKLoginManager();
            
            loginManager.logInWithReadPermissions(["email"], handler: {
                (result, error) in
                
                Settings.sharedInstance.facebookId = FBSDKAccessToken.currentAccessToken().userID;
                self.performSegueWithIdentifier("MainViewController", sender: self);
            });

        }
    }
    
        
//Mark: Twitter APIs
    func twitterLogin(){
     
        if(Twitter.sharedInstance().session() == nil){
            Twitter.sharedInstance().logInWithCompletion {
                session, error in
                
                if (session != nil) {
                    Settings.sharedInstance.twitterId = session.userID;
                    self.performSegueWithIdentifier("MainViewController", sender: self);
                } else {
                    Logging.printLog(NSStringFromClass(self.dynamicType), description: "Error logging to Twitter", dataToPrint: error.localizedDescription)
                }
            }
        }
    }
    
  
    
    func invite(){
        
        var content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: "https://itunes.apple.com/us/app/clear-day-free-formerly-weather/id412489722?mt=8&ign-mpt=uo%3D4");
        FBSDKAppInviteDialog.showWithContent(content, delegate: nil);
        
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        
        println("Invitation completed")
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        println("Invitation canceled")
    }
    
//MARK: Touches Handling
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        if(usernameTextField.isFirstResponder() || passwordtextField.isFirstResponder()){
            usernameTextField.resignFirstResponder();
            passwordtextField.resignFirstResponder();
        }
        
    }

//MARK: UITExtfield delegate methods
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.background = UIImage(named: "InputBGActive");
        return true;
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        textField.background = UIImage(named: "InputBG");
        return true;
    }
    
}

