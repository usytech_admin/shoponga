//
//  SettingsViewController.swift
//  Shoponga
//
//  Created by Elsammak on 9/17/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

//MARK: IBAction methods
    
    @IBAction func backButtonPressed(sender: UIBarButtonItem) {
        
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    
//MARK: Logout methodss
    func facebookLogout(){
        
        if(FBSDKAccessToken.currentAccessToken() != nil){ //User logged in
            
            //log out
            var loginManager: FBSDKLoginManager = FBSDKLoginManager();
            loginManager.logOut();
            Settings.sharedInstance.facebookId = nil;
            
        }
        
    }

    func twitterLogout(){
        
        if(Twitter.sharedInstance().session() != nil){
            Twitter.sharedInstance().logOut();
            Settings.sharedInstance.twitterId = nil;
        }
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "logout"){
            
            facebookLogout();
            twitterLogout();
        }
    }


}
