//
//  SearchViewController.swift
//  Shoponga
//
//  Created by Elsammak on 7/28/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
* :discussion: The main class that displays search view controller and display suggestions
*/


import UIKit
import LMDropdownView

class SearchViewController: UIViewController,UISearchBarDelegate, UISearchControllerDelegate,UITableViewDataSource, UITableViewDelegate,RetailerModelToViewDelegate {

    
    let textCellIdentifier = "TextCell"
    var shopongaViewController: ShopongaViewController!;
    
    //IBoutlets
    @IBOutlet var searchBar : UISearchBar!
    @IBOutlet var retailersTableView : UITableView!;
    @IBOutlet var loadingView : UIView!;
    
    //Views
    var _retailerDropDownMenu: LMDropdownView = LMDropdownView();
    
    private
    var _suggestionsArray : [String] = [];
    var _query = "";
    var _isSearching: Bool = false; //boolean to indicate wether the searching is in progress or not.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.showsBookmarkButton=true;
        
        searchBar.setImage(UIImage(named: "RetailerList"), forSearchBarIcon: .Bookmark, state: .Normal);
        
        self.searchDisplayController?.setActive(true, animated: false);
        searchBar.becomeFirstResponder();
    }

    func setCurrentShopongaViewController(shopongaViewController: ShopongaViewController){
        self.shopongaViewController = shopongaViewController;
    }
    

// MARK: UISearchDisplayController Delagte
    
    func searchDisplayController(controller: UISearchController, shouldReloadTableForSearchString searchString: String!) -> Bool {
        
        _isSearching = true;
        RetailerManager.sharedInstance.searchForSuggestedKeywords(searchBar.text);
        RetailerManager.sharedInstance.currentRetailer.suggestionsDataDelegate = self;
        return true;
    }
    func searchDisplayController(controller: UISearchController, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        
        return true;
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        _isSearching = false;
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func searchBarBookmarkButtonClicked(searchBar: UISearchBar) {
        
        retailersTableView.frame = CGRectMake(self.retailersTableView.frame.origin.x,
            self.retailersTableView.frame.origin.y,
            self.view.bounds.size.width,
            CGFloat(RetailerObjectManager.sharedInstance.retailersList.count) * 44);
        
        self.retailersTableView.reloadData();
        
        
        _retailerDropDownMenu.menuContentView = self.retailersTableView;
        
        // Show/hide dropdown view
        if (_retailerDropDownMenu.isOpen())
        {
            _retailerDropDownMenu.hide();
        }
        else
        {
            _retailerDropDownMenu.showInView(self.view, withFrame: self.view.bounds);
        }
        
    }
    
    
// MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        startsSearch();
    }
    
// MARK: UITableViewDlegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        return 44;
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (tableView == self.searchDisplayController!.searchResultsTableView){
            
            return _suggestionsArray.count;
        }
        else if(tableView == retailersTableView){
            
            return RetailerObjectManager.sharedInstance.retailersList.count;
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if (tableView == self.searchDisplayController!.searchResultsTableView && _suggestionsArray.count>0){
            
            var cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier) as? UITableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: textCellIdentifier)
            }
            
            cell?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7);
            cell?.textLabel?.text = _suggestionsArray[indexPath.row];
            cell?.textLabel?.textColor = UIColor.whiteColor();
            
            return cell!;
        }
        else if (tableView == retailersTableView && RetailerObjectManager.sharedInstance.retailersList.count>0){
            
            var cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier) as? UITableViewCell
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: textCellIdentifier)
            }
            
            cell?.textLabel?.text = RetailerObjectManager.sharedInstance.retailersList[indexPath.row].name;
            
            if(RetailerObjectManager.sharedInstance.retailersList[indexPath.row].name == RetailerManager.sharedInstance.currentRetailer.getName()){
                cell?.accessoryView = UIImageView(image: UIImage(named: "checked"));
            }
            else{
                cell?.accessoryView = UIImageView(image: UIImage(named: "unchecked"));
            }
            cell?.accessoryView?.frame = CGRectMake(0, 0, 30, 30);
            
            return cell!;
        }
            
        let cell:UITableViewCell = UITableViewCell();
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if (tableView == self.searchDisplayController!.searchResultsTableView && _suggestionsArray.count>0){  //Select suggestion
            
            searchBar.text = _suggestionsArray[indexPath.row]
            startsSearch();
            
        }
        else if(tableView == retailersTableView){   //Set current retailer
            
            RetailerManager.sharedInstance.setMainRetailer(RetailerObjectManager.sharedInstance.retailersList[indexPath.row]);
            RetailerManager.sharedInstance.currentRetailer.delegate = self.shopongaViewController;
            
            var retailerChoosen = RetailerManager.sharedInstance.currentRetailer;
            
            retailersTableView.deselectRowAtIndexPath(indexPath, animated: false);
            
            self._retailerDropDownMenu.hide();
            
            startsSearch();
            
            var param: Dictionary<String, AnyObject> = Dictionary<String,AnyObject>();
            param[retailerChoosen.getName()] = 1;
            Flurry.logEvent(RETAILER_SELECTION_KEY, withParameters: param);
            
        }
        
        tableView.reloadData();
        self.navigationController?.popViewControllerAnimated(true);
    }

    
// MARK: Searching Module    
    func startsSearch(){
        
//        println(searchBar.text);
//        println(_query);
        if(searchBar.text.isEmpty  && _query.isEmpty){
            return;
        }
        endSearch();
        !searchBar.text.isEmpty ? (_query = searchBar.text) : (_query = _query);
        RetailerManager.sharedInstance.searchForItem(_query);
        self.searchDisplayController?.setActive(false, animated: true);
        loadingView.alpha = 0.75;
        _isSearching = true;
        self.shopongaViewController.searchQuery = _query;
    }
    func endSearch(){
        
        RetailerManager.sharedInstance.endSearch();
        _suggestionsArray.removeAll(keepCapacity: false);
        loadingView.alpha = 0;
    }
    

// MARK: Data returned
    func autocompleteDataReturned (arr: Array<String>){
        
        _suggestionsArray = arr;
        self.searchDisplayController!.searchResultsTableView.reloadData();
    }
    
// MARK: Touches Handling
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        println("Is searching: \(_isSearching)");
        //Hide search view
        if (!_isSearching){
            self.searchDisplayController?.setActive(false, animated: false);
            self.dismissViewControllerAnimated(true, completion: nil)
        }

    }
}
