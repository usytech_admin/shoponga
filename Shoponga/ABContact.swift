//
//  ABContact.swift
//  Shoponga
//
//  Created by Elsammak on 7/12/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import UIKit

class ABContact: NSObject {
    
    
    var heightOfCell : CGFloat = 30
    
    var isMobileSelected = false
    var isIphoneSelected = false
    var ishomeEmailSelected = false
    var isWorkEmailSelected = false
    var isExpanded = false
    var index = 0  //This is used to know the index of this contact ex.scroll tableview to this contact
    
    //Data
    var name: String!
    
    var mobileNumber: String?{
        didSet{
            if let mobileNumber = self.mobileNumber{
                heightOfCell+=30;
            }
        }
    }
    var iPhoneNumber: String?{
        didSet{
            if let iPhoneNumber = self.iPhoneNumber{
                heightOfCell+=30;
            }
        }
    }
    var homeEmail: String?{
        didSet{
            if let homeEmail = self.homeEmail{
                heightOfCell+=30;
            }
            
        }
    }
    var workEmail: String?{
        didSet{
            if let workEmail = self.workEmail{
                heightOfCell+=30;
            }
        }
    }
    
    func isThereAnyThingSelected()->Bool{
        
        return (isMobileSelected || isIphoneSelected || ishomeEmailSelected || isWorkEmailSelected)
        
    }
    
}

