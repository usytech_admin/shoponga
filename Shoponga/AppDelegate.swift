//
//  AppDelegate.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Fabric
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //Regisetr for notifications
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: .Alert | .Badge | .Sound, categories: nil))
        
        //Start Twitter
        Fabric.with([Twitter()]);
        
        //Start flurry
        Flurry.startSession(FLURRY_KEY);
        Flurry.setCrashReportingEnabled(true)
        
        //Start Instabug
        Instabug.startWithToken(INSTABUG_TOKEN, captureSource:IBGCaptureSourceUIKit, invocationEvent: IBGInvocationEventShake);
        
        initShoponga();
        
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions);
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {

        //Save local
        UserProfileManager.sharedInstance.saveData();
        
        //Save settings
        Settings.sharedInstance.save();

    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        
        let myAlert = UIAlertView()
        myAlert.message = notification.alertBody;
        myAlert.addButtonWithTitle("Ok")
        myAlert.delegate = self
        myAlert.show()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {

        
        if(url.scheme == FACEBOOK_URL_SCHEME){
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation);
        }
        
        
        var query = url.query;
        if(query == nil){
            return true;
        }
        
        
        
        var array = query?.componentsSeparatedByString("&");
        var parametersDict = getURLParameterDict(array!);
        
        if(parametersDict["source"] as! String == "sharing"){
                        
            
            var listToshareID: String = parametersDict["shareListID"] as! String;
//            var shoppingList: ShoppingList = ShoppingList();
//            shoppingList.id = listToshareID;
//            shoppingList.isShared = true;
//            UserProfileManager.sharedInstance.listsArray[listToshareID] = shoppingList
            
            
            CouchbaseManager.sharedInstance.pullDocumentWithIDRemotely(listToshareID);
            
            println("Accepting list with id\(listToshareID)")
//            self.synchingdb();
        }
        
        return true;
    }
// MARK: Init Shoponga
    func initShoponga(){
        
        CouchbaseManager.sharedInstance.compactDB();
        self.synchingdb();
        Settings.sharedInstance = Settings.loadData();
    }
    
    func synchingdb(){
        CouchbaseManager.sharedInstance.syncdb();
    }
    
    
    func getURLParameterDict(array: [String]) -> Dictionary<String, AnyObject>{
        
        var dictionary: Dictionary<String, AnyObject> = Dictionary<String, AnyObject>();
        
        for para in array{
            var paraArray = para.componentsSeparatedByString("=");
            dictionary[paraArray[0]] = paraArray[1];
        }
        
        return dictionary;
        
    }
    
    
}

