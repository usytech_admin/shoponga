//
//  GlobalConstants.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//


/// This class Contains Global variables to be used anywhere in the code.


import UIKit;

//################################# Debug flags #################################

let PRINT_URL_REQUESTS = 0;
let PRINT_URL_RESPONSES = 0;


//################################# Configurations parameters #################################


let ENABLE_CACHE: Bool = true;


//################################# General Keys #################################

let MAX_NUMBER_BATCH_REQUEST = 20;
let SHOPONGA_URLSCHEME = "Shoponga://?" //Used to open Shoponga from another app or even from safari (Our main use is to share lists with friends)
let FACEBOOK_URL_SCHEME = "fb100955840255888"
//################################# Amazon Keys #################################

// For documentation, please refer to http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CommonRequestParameters.html   and http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_OperationListAlphabetical.html

// For the autocomplete part, we use Amazon. Google offers a similar service at http://shreyaschand.com/blog/2013/01/03/google-autocomplete-api/ But it's not recommended as Google will display a "Non-product" items, (ex. Egypt).

let AWS_ACCESS_KEY_ID = "AKIAISZBMHKCDMSGKWTA"
let AWS_SECRET_KEY = "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"
let AMAZON_SERVICE_URL = "http://ecs.amazonaws.com/onca/xml?Service=AWSECommerceService"
let AMAZON_AUTOCOMPLETE_URL = "http://completion.amazon.com/search/complete"
let AMAZON_API_VERSION = "2011-08-01"
let ASSOCIATE_TAG = "PutYourAssociateTagHere"  //USED TO MAKE REVENUE


//################################# Walmart Keys #################################

let WALMART_API_KEY = "trvct3vcg5ckx9bsan8pcn64"
let WALMART_SERVICE_URL = "http://api.walmartlabs.com/v1/";


//################################# BestBuy Keys #################################

let BESTBUY_SERVICE_URL = "http://api.remix.bestbuy.com/v1/";
let BESTBUY_API_KEY = "qpmedqe42464spue2bz7vum9";


//################################# Flurry Keys #################################

let FLURRY_KEY = "8CVTCXGHFDMZW8SNT5Y3";

//################################# Flurry Events #################################

let RETAILER_SELECTION_KEY = "SELECTED_RETAILER";


//################################# CouchDB keys #################################
let COUCH_DB_NAME = "shopongadb"
let COUCH_DB_REMOTELY_URL = NSURL(string: "http://127.0.0.1:5984/shopongadb/")!
let COUCH_DB_USERNAME = "ShopongaDB";
let COUCH_DB_PASSWORD = "ShopongaDB";

//################################# Instabug Keys #################################
let INSTABUG_TOKEN = "244433a03a15c57c7eaf627bef20678f";

//################################# Constants #################################
let GRAY_COLOR = UIColor(red: 61/255, green: 73/255, blue: 83/255, alpha: 1);
let LIGHT_GRAY_COLOR = UIColor(red: 110/255, green: 110/255, blue: 110/255, alpha: 1);
let PINK_COLOR = UIColor(red: 193/255, green: 15/255, blue: 90/255, alpha: 1);
//################################# Useful extensions #################################

//Load images async
extension UIImageView {
    public func imageFromUrlAsync(urlString: String) {
        
        
        if (ENABLE_CACHE){
            var cachedData: NSData? = CacheManager.sharedInstance.cache.objectForKey(urlString) as? NSData;
            if cachedData != nil {
                self.image = UIImage(data: cachedData!);
                return;
            }
        }

        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                self.image = UIImage(data: data)
                
            }
        }
    }
}


extension NSDictionary{
    
    var toSwiftDictionary:Dictionary<String, AnyObject> {
        var swiftDict : Dictionary<String,AnyObject!> = Dictionary<String,AnyObject!>()
        for key : AnyObject in self.allKeys {
            let stringKey = key as! String
            if let keyValue: AnyObject = self.valueForKey(stringKey){
                swiftDict[stringKey] = keyValue
            }
        }
        
        return swiftDict;
    }
    
}
extension String {
    var html2AttributedString:NSAttributedString {
        return NSAttributedString(data: dataUsingEncoding(NSUTF8StringEncoding)!, options:[NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil, error: nil)!
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    var isBlank: Bool {
        let trimmed = self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return trimmed.isEmpty
    }
    var isValidEmail: Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}"
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(self)
        return result
    }
    
}

extension Array {
    func contains<T where T : Equatable>(obj: T) -> Bool {
        return self.filter({$0 as? T == obj}).count > 0
    }
}
