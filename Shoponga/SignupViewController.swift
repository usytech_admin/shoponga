//
//  SignupViewController.swift
//  Shoponga
//
//  Created by Elsammak on 8/13/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/**
    The Registeration View controller
*/
import Foundation
class SignupViewController: UIViewController, UITextFieldDelegate{
    
    //IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
// MARK: IBAction Methods
    @IBAction func createNewAccountButtonPressed(sender: UIButton) {
        
        //Make sure of contents (Filteration)
        
        //Check empty values
        if(nameTextField.text.isEmpty || emailTextField.text.isEmpty || passwordTextField.text.isEmpty || confirmPasswordTextField.text.isEmpty){
            
            return;
        }
        
        //Check email validation
        if(!emailTextField.text.isValidEmail){
            return;
        }
        
        //Check matching password
        if(passwordTextField.text != confirmPasswordTextField.text){
            return;
        }
        
// TODO: Register module
        
    }
    
    @IBAction func backButtonPressed(sender: UIBarButtonItem) {
        
        navigationController?.popViewControllerAnimated(true);
    }
    
    
//MARK: UITExtfield delegate methods
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.background = UIImage(named: "InputBGActive");
        return true;
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        textField.background = UIImage(named: "InputBG");
        return true;
    }
}
