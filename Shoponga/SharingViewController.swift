//
//  SharingViewController.swift
//  Shoponga
//
//  Created by Elsammak on 8/25/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/// This class is to show current users who share the same list with you, and also give the ability to share it with others.
import Foundation

class SharingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,addressBookProtocol {
    
    //Constants
    let textCellIdentifier = "InviteFriendsCell";
    
    
    var shoppingList: ShoppingList!;
    
    //Privates
    private
    var _isAddressBookShown: Bool = false;
    
    //IBOutlets
    @IBOutlet weak var friendsTableView: UITableView!
    
    //ViewControllers
    var addressBookViewController = AddressBookViewController();
    
    
//MARK: Init methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        friendsTableView.registerNib(UINib(nibName: "InviteFriendTableViewCell", bundle: nil), forCellReuseIdentifier: textCellIdentifier)
        
        addressBookViewController = AddressBookViewController(nibName: "AddressBookViewController", bundle: nil);        
        addressBookViewController.delegate = self;
    }
    
    
// MARK: UITableViewDlegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 44;
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier) as? InviteFriendTableViewCell
        
        if cell == nil {
            cell = InviteFriendTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: textCellIdentifier)
        }
        
        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    

//MARK: IBAction methods
    @IBAction func inviteFriendsButtonPressed(sender: UIButton) {
    
        showOrHideAddressBook();
    }
    
    @IBAction func backButtonPressed(sender: UIBarButtonItem) {
        
        navigationController?.popViewControllerAnimated(true);
    }
    
// MARK: Addressbook delegate methods
    
    func showOrHideAddressBook(){
        
        if (_isAddressBookShown) { //Hide addressbook
            
            _isAddressBookShown = false;
            
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
        }
        else{ //Show address book
            
            _isAddressBookShown = true;
            
            addressBookViewController.listId = shoppingList.id;
            addressBookViewController.startOpenContactsBook()
            
            self.navigationController?.presentViewController(addressBookViewController, animated: true, completion: nil);
            
            
        }
    }
    
}