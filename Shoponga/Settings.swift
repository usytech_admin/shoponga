//
//  Settings.swift
//  Shoponga
//
//  Created by Elsammak on 8/10/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation

class Settings: NSObject, NSCoding {
    
    
    //Parameters to save
    var twitterId: String?
    var facebookId: String?
    var userId: String?
    
    static var sharedInstance = Settings(); //Singlton Instance
    


    
    
    
    func getUserId() -> String?{
        
        if(twitterId == nil && facebookId == nil && userId == nil){ //All ids are nil
            return nil;
        }
        else if(twitterId != nil){
            return twitterId;
        }
        else if(facebookId != nil){
            return facebookId;
        }
        return userId;
    }
    
// MARK: NSCoding
    
    @objc required convenience init(coder decoder: NSCoder) {
        
        
        self.init();
        
        self.twitterId = decoder.decodeObjectForKey("TwitterID") as? String;
        self.facebookId = decoder.decodeObjectForKey("FacebookID") as? String;
    }
    
    @objc func encodeWithCoder(coder: NSCoder) {
        
        coder.encodeObject(self.twitterId, forKey: "TwitterID");
        coder.encodeObject(self.facebookId, forKey: "FacebookID");
    }
    
    
    
// MARK: Saving and Loading
    func save() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "Settings")
    }
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("Settings")
    }
    
    class func loadData() -> Settings {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("Settings") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Settings
        }
        return Settings()
    }
    
    
}