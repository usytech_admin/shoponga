//
//  AddressBookEntryCell.swift
//  Shoponga
//
//  Created by Elsammak on 7/12/15.
//  Copyright (c) 2015 USYTech. All rights reserved.


import UIKit


protocol AddressBookCellProtocol{
    
    func emailSelected(isSelected: Bool);
    func mobileSelected(isSelected: Bool);
    
}

class AddressBookEntryCell: UITableViewCell {
    
    
    // MARK: ======Properties======
    
    @IBOutlet var contactName : UILabel!
    @IBOutlet var mobileButton : UIButton!
    @IBOutlet var iPhoneButton : UIButton!
    @IBOutlet var workEmailButton : UIButton!
    @IBOutlet var homeEmailButton : UIButton!
    
    @IBOutlet var mobileView : UIView!
    @IBOutlet var iPhoneView : UIView!
    @IBOutlet var workEmailView : UIView!
    @IBOutlet var homeEmailView : UIView!
    
    @IBOutlet var nameMarkImageView : UIImageView!
    @IBOutlet var mobileMarkImageView : UIImageView!
    @IBOutlet var iPhoneMarkImageView : UIImageView!
    @IBOutlet var workEmailMarkImageView : UIImageView!
    @IBOutlet var homeEmailMarkImageView : UIImageView!
    
    @IBOutlet var mobileViewHeightConstant : NSLayoutConstraint!
    @IBOutlet var iPhoneViewHeightConstant : NSLayoutConstraint!
    @IBOutlet var workEmailViewHeightConstant : NSLayoutConstraint!
    @IBOutlet var homeEmailViewHeightConstant : NSLayoutConstraint!
    
    
    var delegate :AddressBookCellProtocol!
    
    var contact: ABContact!{
        didSet{
            
            contactName.text = self.contact.name
            
            setMobileNumber(self.contact.mobileNumber)
            setIPhoneNumber(self.contact.iPhoneNumber)
            setHomeEmail(self.contact.homeEmail)
            setWorkEmail(self.contact.workEmail)
        }
    }
    
    var isMobileButtonSet = false
    var isIphoneButtonSet = false
    var isWorkEmailButtonSet = false
    var isHomeEmailButtonSet = false
    
    // MARK: ======init======
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
        //Check if this cell contaisn selected items
        if (!contact.isExpanded && contact.isThereAnyThingSelected()){
            
            nameMarkImageView.alpha = 1;
        }
        else{
            nameMarkImageView.alpha = 0;
        }
        
    }
    
    
// MARK: ======Setting functions======
    func setMobileNumber (mobileNumber: String?) {

        if (mobileNumber != nil){
            
            //Set frame of button
            mobileViewHeightConstant.constant = 30;
            
            //Set title
            mobileButton.setTitle(mobileNumber, forState: UIControlState.Normal);
            
            //Raise flag
            isMobileButtonSet = true;
        }
        else{
            
            mobileViewHeightConstant.constant = 0;
            
            //Raise flag
            isMobileButtonSet = false;
        }
        
    }
    
    func setIPhoneNumber (iPhoneNumber: String?) {
        
        if (iPhoneNumber != nil){
            
            //Set frame of button
            iPhoneViewHeightConstant.constant = 30;
            
            //Set title
            iPhoneButton.setTitle(iPhoneNumber, forState: UIControlState.Normal);
            
            //Raise flag
            isIphoneButtonSet = true;
        }
        else{
            
            //Set frame of button
            iPhoneViewHeightConstant.constant = 0;
            
            //Raise flag
            isIphoneButtonSet = false;
        }
        
        
        
        
    }
    
    func setHomeEmail (homeEmail: String?) {
        
        if (homeEmail != nil){
            
            //Set frame of button
            homeEmailViewHeightConstant.constant = 30;
            
            //Set title
            homeEmailButton.setTitle(homeEmail, forState: UIControlState.Normal);
            
            //Raise flag
            isHomeEmailButtonSet = true;
        }
        else{
            
            //Set frame of button
            homeEmailViewHeightConstant.constant = 0;
            
            //Raise flag
            isHomeEmailButtonSet = false;
        }
        
        
    }
    
    func setWorkEmail (workEmail: String?) {
        
        
        if (workEmail != nil){
            
            //Set frame of button
            workEmailViewHeightConstant.constant = 30;
            
            //Set title
            workEmailButton.setTitle(workEmail, forState: UIControlState.Normal);
            
            //Raise flag
            isWorkEmailButtonSet = true;
            
        }
        else{
            
            //Set frame of button
            workEmailViewHeightConstant.constant = 0;
            
            //Raise flag
            isWorkEmailButtonSet = false;
            
        }
    }
    
    func adjustCellView(){
        
        
        if(!self.contact.isExpanded){
            
            mobileViewHeightConstant.constant = 0;
            iPhoneViewHeightConstant.constant = 0;
            homeEmailViewHeightConstant.constant = 0;
            workEmailViewHeightConstant.constant = 0;
            return;
        }
        
        if !isMobileButtonSet{ //First button: Mobile button
            
            mobileViewHeightConstant.constant = 0;
        }
        
        if !isIphoneButtonSet{  //Second button: iPhone button
            
            iPhoneViewHeightConstant.constant = 0;
        }
        
        if !isHomeEmailButtonSet{  //Third button: Home Email button
            
            homeEmailViewHeightConstant.constant = 0;
        }
        
        if !isWorkEmailButtonSet{  //Forth button: Work Email button
            
            workEmailViewHeightConstant.constant = 0;
        }
        
        
        //Setting views
        if contact.isMobileSelected{
            
            mobileButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            mobileMarkImageView.image = UIImage(named: "checked")
        }
        else{
            
            mobileButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            mobileMarkImageView.image = UIImage(named: "unchecked")
        }
        
        
        if contact.isIphoneSelected{
            
            iPhoneButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            iPhoneMarkImageView.image = UIImage(named: "checked")
        }
        else{
            
            iPhoneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            iPhoneMarkImageView.image = UIImage(named: "unchecked")
        }
        
        if contact.ishomeEmailSelected{
            
            homeEmailButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            homeEmailMarkImageView.image = UIImage(named: "checked")
        }
        else{
            
            homeEmailButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            homeEmailMarkImageView.image = UIImage(named: "unchecked")
        }
        
        if contact.isWorkEmailSelected{
            
            workEmailButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            workEmailMarkImageView.image = UIImage(named: "checked")
        }
        else{
            
            workEmailButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            workEmailMarkImageView.image = UIImage(named: "unchecked")
        }
        
    }
    
    
    // MARK: ======IBAction methods======
    
    @IBAction func mobileButtonPressed(){
        
        if contact.isMobileSelected{
            
            mobileButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            contact.isMobileSelected = false
            mobileMarkImageView.image = UIImage(named: "unchecked")
            
        }
        else{
            
            mobileButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            contact.isMobileSelected = true
            mobileMarkImageView.image = UIImage(named: "checked")
        }
        
        delegate.mobileSelected(contact.isMobileSelected);
    }
    
    @IBAction func iPhoneButtonPressed(){
        
        if contact.isIphoneSelected{
            
            iPhoneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            contact.isIphoneSelected = false
            
            iPhoneMarkImageView.image = UIImage(named: "unchecked")
        }
        else{
            
            iPhoneButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            contact.isIphoneSelected = true
            
            iPhoneMarkImageView.image = UIImage(named: "checked")
        }
        
        delegate.mobileSelected(contact.isIphoneSelected);
    }
    
    @IBAction func workEmailButtonPressed(){
        
        if contact.isWorkEmailSelected{
            
            workEmailButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            contact.isWorkEmailSelected = false
            
            workEmailMarkImageView.image = UIImage(named: "unchecked")
        }
        else{
            
            workEmailButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            contact.isWorkEmailSelected = true
            
            workEmailMarkImageView.image = UIImage(named: "checked")
        }
        
        delegate.emailSelected(contact.isWorkEmailSelected);
    }
    
    @IBAction func homeEmailButtonPressed(){
        
        if contact.ishomeEmailSelected{
            
            homeEmailButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            contact.ishomeEmailSelected = false
            
            homeEmailMarkImageView.image = UIImage(named: "unchecked")
        }
        else{
            
            homeEmailButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            contact.ishomeEmailSelected = true
            
            homeEmailMarkImageView.image = UIImage(named: "checked")
        }
        
        delegate.emailSelected(contact.ishomeEmailSelected);
    }
    
    deinit{
        
    }
    
}
