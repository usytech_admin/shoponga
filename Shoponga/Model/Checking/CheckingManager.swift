//
//  CheckingManager.swift
//  Shoponga
//
//  Created by Elsammak on 6/30/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//


/*!*
    This is the class responsible for checking periodically for item prices and fire a notifications in case there is any change
*/

import Foundation
import UIKit
class CheckingManager: RetailerBatchDelegate {
    
    static var sharedInstance = CheckingManager(); //Singlton Instance
    
    var itemsArray: [Item] = [];
    
    private init() {
        // Private initialization to ensure just one instance is created.
    }
    
    /*!
        Checking for all items added and notify user if any one of them has a sale.
    */
    func updateItems(){
  
        for retailerObject in RetailerObjectManager.sharedInstance.retailersList{
            
            RetailerManager.sharedInstance.setSilentRetailer(retailerObject);
            RetailerManager.sharedInstance.silentRetailer?.batchDataDelegate = self;
            
            let itemsListForRetailer: [String]! = RetailerObjectManager.sharedInstance.getListOfItemsForRetailer(retailerObject.id)?.keys.array;
            
            for var index = 0 ; index < itemsListForRetailer?.count ; index = index + MAX_NUMBER_BATCH_REQUEST {
                
                var numberOfElements = (itemsListForRetailer.count - index > MAX_NUMBER_BATCH_REQUEST ? MAX_NUMBER_BATCH_REQUEST : itemsListForRetailer.count - index);
                var listString = getListOfIDsAsString (itemsListForRetailer!, startingIndex: index, numberOfElements: numberOfElements);
                
                RetailerManager.sharedInstance.updateItems(listString);
            }
        }
        
    }
    
        
    func batchResultsReturned(itemsList: [Item], retailerObject: RetailerObject){

        itemsArray = itemsList;
        
        //Start checking for this retailer
        compareOldWithUpdatedPricesForRetailer(retailerObject);
        
    }
    
    func getListOfIDsAsString(keysArray: [String], startingIndex: Int, numberOfElements: Int) -> String{
        
        var listString = "";
        for var i=startingIndex;i<startingIndex + numberOfElements; i++ {
            
            if(listString != ""){
                listString += ",";
            }
            listString += keysArray[i];
        }
        return listString;
    }
    
    
    func compareOldWithUpdatedPricesForRetailer(retailerObject: RetailerObject){
        
        for newItem in itemsArray {
            
            if(newItem.price < retailerObject.itemsList[newItem.id]?.price){
                //Generate push notification
                fireNotification(newItem, retailerObject: retailerObject);
                
                //Update old item
                retailerObject.itemsList[newItem.id]? = newItem;
            }
            else{
                println("Same price for \(newItem.name) at \(retailerObject.name)");
//                fireNotification(newItem, retailerObject: retailerObject);
            }
            
        }
    }
    
    func fireNotification(item: Item, retailerObject: RetailerObject){
        
        // create a corresponding local notification

        var notification = UILocalNotification();
        notification.alertBody = "Item \"\(item.name)\" is now \(item.price)" // text that will be displayed in the notification
        notification.alertAction = "Check Item" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.fireDate = NSDate(timeIntervalSinceNow: 1) // todo item due date (when notification will be fired)
        notification.soundName = UILocalNotificationDefaultSoundName // play default sound
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
}
