//
//  Item.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//
/*!
    The item class, All items returned from multipl eretailers must be mapped to a single one for standardized use.
*/

import Foundation

class Item: Serializable, NSCoding {

    var id: String!;  ///Item Id
    var name: String = ""{  ///Item Name
        
        didSet{
            name = name.html2AttributedString.string;
        }

    }
    
    var desc: String?{ ///Item Description
        
        didSet{
            if(desc != nil){
                desc = desc!.html2AttributedString.string;
            }
        }
        
    }
    
    
    var category: String!; ///Item category
    var smallImage : String?;  ///Item small image
    var largeImage : String?;  ///Item large image
    var retailerId: String!;  ///From which retailer is this item
    
    var price: Float32!;  ///Item price in USD
    var isUpdated : Bool = false; ///Default is false, if user request item details this variable turns to be true to avoid loading everytime
    var listID: String?  //The list which hold this item
    
    override init (){

    }
    
    func getPriceString() -> String{
        
        return "$" + self.price.description;
    }
    
    func getListId() ->String?{
        return listID;
    }
// MARK: NSCoding
    
    @objc required convenience init(coder decoder: NSCoder) {
        
        self.init();
        
        self.id = decoder.decodeObjectForKey("ID") as! String;
        self.retailerId = decoder.decodeObjectForKey("RetailerID") as! String;
        self.name = decoder.decodeObjectForKey("Name") as! String;
        self.price = decoder.decodeFloatForKey("Price");
        self.category = decoder.decodeObjectForKey("Category") as! String;
        self.smallImage = decoder.decodeObjectForKey("SmallImage") as? String;
        self.largeImage = decoder.decodeObjectForKey("LargeImage") as? String;
        self.desc = decoder.decodeObjectForKey("Description") as? String;
        self.listID = decoder.decodeObjectForKey("ListID") as? String;
    }
    
    @objc func encodeWithCoder(coder: NSCoder) {
        
        coder.encodeObject(self.id, forKey: "ID")
        coder.encodeObject(self.retailerId, forKey: "RetailerID")
        coder.encodeObject(self.name, forKey: "Name")
        coder.encodeFloat(self.price, forKey: "Price");
        coder.encodeObject(self.category, forKey: "Category")
        coder.encodeObject(self.smallImage, forKey: "SmallImage")
        coder.encodeObject(self.largeImage, forKey: "LargeImage")
        coder.encodeObject(self.desc, forKey: "Description")
        coder.encodeObject(self.listID, forKey: "ListID");
    }

}