//
//  UserProfileManager.swift
//  Shoponga
//
//  Created by Elsammak on 7/7/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//


/*!
    The class of user profile, it includes all users data like personal data and shopping lists, items.
*/

import Foundation

class UserProfileManager: NSObject, NSCoding {
    
    var userId : String!;
    var userName : String!
    var listsArray : Dictionary<String, ShoppingList>!;
    
    static var sharedInstance = UserProfileManager(); //Singlton Instance
    
   override private init() {
        // Private initialization to ensure just one instance is created.
        self.userId = UIDevice.currentDevice().identifierForVendor.UUIDString;
        userName = "Hakim"
    }
    
    
// MARK: NSCoding    
    @objc required convenience init(coder decoder: NSCoder) {
        
        self.init();
        
        self.userId = decoder.decodeObjectForKey("userID") as! String;
        self.userName = decoder.decodeObjectForKey("UserName") as! String;
        self.listsArray = decoder.decodeObjectForKey("ListsArray") as! Dictionary<String, ShoppingList>;
        
    }
    
    @objc func encodeWithCoder(coder: NSCoder) {
        
        coder.encodeObject(self.userId, forKey: "userID");
        coder.encodeObject(self.userName, forKey: "UserName");
        coder.encodeObject(self.listsArray, forKey: "ListsArray")
    }

    
    /*!
        Save user's personal and shopping data
    */
    func saveData(){
       
        listsArray = ShoppingListManager.sharedInstance.listsArray; //Updata data
        var data = NSKeyedArchiver.archivedDataWithRootObject(self);
        CouchbaseManager.sharedInstance.saveAttachement(data, name: NSStringFromClass(self.dynamicType));
        
        //Sync shared shopping lists
        for shoppingList in listsArray.values.array{
            if(shoppingList.isShared == true){ //Update this list
                var shoppingListDic = shoppingList.toDictionary().toSwiftDictionary;
                CouchbaseManager.sharedInstance.saveDataDictionary(shoppingListDic, key: shoppingList.id);
            }
        }
    }

    /*!
    Load user's personal and shopping data
    */
    
    func loadData(){
        
        if let data: AnyObject = CouchbaseManager.sharedInstance.loadAttachementWithName(NSStringFromClass(self.dynamicType)){

            self.userId =  data.userId;
            self.userName =  data.userName;
            self.listsArray =  data.listsArray;
            
        }
        else{
            return;
        }

        if (listsArray.values.array.count == 0){
            return;
        }
        //Check for shared lists
        
        for index in 0...listsArray.values.array.count-1{
            
            var shoppingList = listsArray.values.array[index];
                if let newList = CouchbaseManager.sharedInstance.loadDataDictionary(shoppingList.id){
                
                    shoppingList = convertDictionaryToShoppingList(newList);
                }                
                
                ShoppingListManager.sharedInstance.listsArray[shoppingList.id] = shoppingList;
        }
    }
    
    /*!
        Used to convert Dictionary<String, AnyObject> to ShoppingList
    
        :param: dictionary The dictionary<String, AnyObject> to deal with
    
        :returns: Shopping list object
    */
    
    func convertDictionaryToShoppingList(dictionary: Dictionary<String, AnyObject>) -> ShoppingList{
        
        var shoppingList: ShoppingList = ShoppingList();
        for key in dictionary.keys.array{
            switch key{
            case "budget":
                shoppingList.budget = dictionary[key] as! Float32;
            case "id":
                shoppingList.id = dictionary[key] as! String;
            case "name":
                shoppingList.name = dictionary[key] as! String;
            case "isShared":
                shoppingList.isShared = dictionary[key] as! Bool;
            case "itemsArray":
                
                let items = dictionary[key] as! NSArray
                for item in items {
                    
                    var dic: Dictionary<String, AnyObject> = (item as! NSDictionary).toSwiftDictionary;
                    shoppingList.itemsArray.append(convertDictionaryToItem(dic))
                }
                
            default:
                break;
            }
        }
        
            return shoppingList;
    }
    
    
    /*!
    Used to convert Dictionary<String, AnyObject> to Item
    
    :param: dictionary The dictionary<String, AnyObject> to deal with
    
    :returns: Item object
    */
    
     func convertDictionaryToItem(dictionary: Dictionary<String, AnyObject>) -> Item{
        
        var item: Item = Item();
        
        for key in dictionary.keys.array{
            switch key{
            case "id":
                item.id = dictionary[key] as! String;
            case "smallImage":
                item.smallImage = dictionary[key] as? String;
            case "category":
                item.category = dictionary[key] as! String;
            case "isUpdated":
                item.isUpdated = dictionary[key] as! Bool;
            case "name":
                item.name = dictionary[key] as! String;
            case "largeImage":
                item.largeImage = dictionary[key] as? String;
            case "retailerId":
                item.retailerId = dictionary[key] as! String;
            case "desc":
                item.desc = dictionary[key] as? String;
            case "price":
                item.price = dictionary[key] as! Float32;
            default:
                break;
            }
        }
        
        return item;
        
    }
}