//
//  CategoriesManager.swift
//  Shoponga
//
//  Created by Elsammak on 7/6/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/// This class is for managing categories
import Foundation

class CategoriesManager {
    
    var categoriesList = [Category]();
    
    
    static var sharedInstance = CategoriesManager(); //Singlton Instance
    
    private init() {
        // Private initialization to ensure just one instance is created.
        
     
        initListOfCategories();
    }
    
    
    
    
    /**
        Initialize with list of retailers of SupportedRetailer.plist
    */
    func initListOfCategories(){
        
        if(categoriesList.count == 0){
            
            var myDict: NSDictionary?
            if let path = NSBundle.mainBundle().pathForResource("Categories", ofType: "plist") {
                myDict = NSDictionary(contentsOfFile: path)
            }
            if let dict = myDict {
                
                var arrayOfCategories : Dictionary<String, Dictionary<String, AnyObject>> = dict as! Dictionary<String, Dictionary<String, AnyObject>>;
                
                for category in arrayOfCategories{
                    
                    
                    var name : String = category.0;
                    var icon: String? = category.1["Icon"] as? String;
                    var keywords: String? = category.1["Keywords"] as? String;
                    
                    var category: Category = Category();

                    category.icon = icon;
                    category.name = name;
                    category.keyWords = keywords;
                    
                    categoriesList.append(category);
                }
                
            }
            
        }
    }
    /**
     Get array of categories names
    
     :returns: Array of categories names
    */
    func getListOfCategoriesNames() -> [String]{
        
        return categoriesList.map({"\($0.name)"});
        
    }
    
}
