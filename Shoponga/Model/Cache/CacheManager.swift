//
//  CacheManager.swift
//  Shoponga
//
//  Created by Elsammak on 6/23/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    The cache manager class is responsible for caching objects for later use
*/

import Foundation

class CacheManager {
    
    static let sharedInstance = CacheManager();
    var cache : NSCache = NSCache();
    
    private init() {
        // Private initialization to ensure just one instance is created.
    }
}