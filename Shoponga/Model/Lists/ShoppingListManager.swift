//
//  ShoppingListManager.swift
//  Shoponga
//
//  Created by Elsammak on 6/22/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    The ShoppingListManager class is the manager of all shopping list by user. It holds an Array of all lists to control them
*/
import Foundation

class ShoppingListManager {
    
    static var sharedInstance = ShoppingListManager(); //Singlton Instance
    var listsArray : Dictionary<String, ShoppingList> = Dictionary<String, ShoppingList>();  //Array of shopping list
    
    private init(){
        var defaultShoppingList = ShoppingList(name: "Default list", budget: 1000);
        defaultShoppingList.id = "0";
        defaultShoppingList.order = 0;
        listsArray[defaultShoppingList.id] = defaultShoppingList;
        
    }
    /*!
        This Method is to create the id of the upcoming list
    
        :returns: The new list ID
    */
    func generateNextId() -> String{
        
        return stringFromTimeInterval(NSDate().timeIntervalSince1970) as String;
    }
    
    
    func getDefaultShoppingList() -> ShoppingList{
        
        return listsArray["0"]!;
    }
    
    /**
    Get shopping list with the specified id
    
    :param: id Id of the list to retrive
    :returns: the shopping list with this id
    */
    func getShoppingList(id: String) -> ShoppingList?{
        return listsArray[id];
    }
    
    /**
    Delete item from the giving shopping list
    
    :param: id Id of the list to retrive
    :returns: the shopping list with this id
    */
    func removeItem(id: String, itemToDelete: Item){
        
        var shoppingList: ShoppingList = self.getShoppingList(id)!;
        for index in 0..<shoppingList.itemsArray.count{
            if(shoppingList.itemsArray[index].id == itemToDelete.id){
                shoppingList.itemsArray.removeAtIndex(index);
                listsArray[id] = shoppingList;
                break;
            }
        }        
    }
    
    
    /**
    Modify shoping list name
    
     :param: id Shopping list ID
     :param: newName New Shopping list name
    */
    func editShoppingListName(id: String, newName: String){

        var shoppingList = getShoppingList(id);
        shoppingList?.name = newName;
        listsArray[id] = shoppingList;
    }
    
    /**
    Modify shoping list budget
    
     :param: id Shopping list ID
     :param: newBudget New Shopping list budget
    */
    func editShoppingListBudget(id: String, newBudget: Float32){
        
        var shoppingList = getShoppingList(id);
        shoppingList?.budget = newBudget;
        listsArray[id] = shoppingList;
    }
    
    /**
        Create a new shopping list and add it to the array
    
        :param: name Name of the list
        :param: budget Budget o the newely created array
    
        :returns: The created shopping list.
    */
    func createNewShoppingList(name: String, budget: Float32) -> ShoppingList{
        
        var newShoppingList: ShoppingList = ShoppingList(name: name, budget: budget);
        newShoppingList.id = UserProfileManager.sharedInstance.userId+String(generateNextId());
        newShoppingList.order = listsArray.count + 1;
        listsArray[newShoppingList.id] = newShoppingList;
        return newShoppingList;
    }
    /**
     Check if this item is already added to any list
    
     :param: id The id of the item
     :returns: The item's list ID or nil if not added
    */
    
    func isItemAdded(id: String) -> String?{
        
        for list in listsArray.values.array{
            for item in list.itemsArray{
                if(item.id == id){
                    return list.id;
                }
            }
        }
        return nil;
    }
    
    /**
        Delete a shopping list using its id
        :param: name Name of the list
        :param: budget Budget o the newely created array
    */
    func deleteShoppingList(id: String){
        listsArray.removeValueForKey(id);
        
        var index: Int = 0;
    }
    
    func swapListWithId(firstId: String, withListWithId: String){
        
        var temp = listsArray[firstId]?.order;
        listsArray[firstId]?.order = listsArray[withListWithId]?.order;
        listsArray[withListWithId]?.order = temp;
    }
    
    
    func getOrderedShoppingList() -> [ShoppingList]{

        var orderedArray: [ShoppingList] = listsArray.values.array;
        
        orderedArray.sort({$0.order < $1.order})
        
        return orderedArray;
    }
    func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        
        var ti = NSInteger(interval)
        return NSString(format: "%d",ti);
    }
    
// MARK: Saving and Loading
    func save() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "ShoppingListManager")
    }
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("ShoppingListManager")
    }
    
    class func loadSaved() -> ShoppingListManager {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("ShoppingListManager") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as! ShoppingListManager
        }
        return ShoppingListManager()
    }
        
    
}
