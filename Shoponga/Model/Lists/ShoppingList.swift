//
//  ShoppingList.swift
//  Shoponga
//
//  Created by Elsammak on 6/22/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//


/*!
    The Shopping list class
    @param name: The name of the shopping list
    @param budget: Budget assigned to this list
*/
import Foundation

class ShoppingList:Serializable , NSCoding{
    
    var id: String!   ///List id
    var name: String; ///Name of list
    var budget: Float32 = 0;  ///Budget of the list
    var itemsArray : [Item] = []; ///Array of items
    var isShared: Bool! = false   ///Boolean indicates whether the list is shared or not
    var order: Int!; //The order of the shooping list (useful in displaying)
    


    
    init(name: String, budget: Float32){
        
        self.name = name;
        self.budget = budget;
    }
    
    
    override init() {
        
        name = "";
        budget = 0;
    }
    
// MARK: NSCoding
    
    @objc required convenience init(coder decoder: NSCoder) {
        

        self.init();
        
        self.id = decoder.decodeObjectForKey("ID") as! String;
        self.name = decoder.decodeObjectForKey("Name") as! String;
        self.budget = decoder.decodeFloatForKey("Budget");
        self.itemsArray = decoder.decodeObjectForKey("ItemsArray") as! [Item];
        self.isShared = decoder.decodeBoolForKey("IsShared");
        self.order = decoder.decodeIntegerForKey("Order");
    }
    
    @objc func encodeWithCoder(coder: NSCoder) {
        
        coder.encodeObject(self.id, forKey: "ID");
        coder.encodeObject(self.name, forKey: "Name");
        coder.encodeFloat(self.budget, forKey: "Budget")
        coder.encodeObject(self.itemsArray, forKey: "ItemsArray")
        coder.encodeBool(self.isShared, forKey: "IsShared");
        coder.encodeInteger(self.order, forKey: "Order");
    }    
    
}