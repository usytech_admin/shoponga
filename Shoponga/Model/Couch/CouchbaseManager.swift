//
//  CouchbaseManager.swift
//  Shoponga
//
//  Created by Elsammak on 6/30/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation
class CouchbaseManager: NSObject {
    
    private var _database: CBLDatabase!;
    private var _manager: CBLManager!
    
    private var _push: CBLReplication!
    private var _pull: CBLReplication!

    private var _documentIdToPull: String?;
    static let sharedInstance = CouchbaseManager();
    
    private override init() {
        // Private initialization to ensure just one instance is created.
        
        var error: NSError?;
        var authenticator = CBLAuthenticator.basicAuthenticatorWithName(COUCH_DB_USERNAME, password: COUCH_DB_PASSWORD);
        
        self._manager = CBLManager.sharedInstance();
        if(self._manager == nil){
            Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot create shared instance of CBLManager", dataToPrint: "");
        }
        self._database = _manager.databaseNamed(COUCH_DB_NAME, error: &error);
        
        // Initialize replication:
        _push = _database.createPushReplication(COUCH_DB_REMOTELY_URL)
        _pull = _database.createPullReplication(COUCH_DB_REMOTELY_URL)
        _push.authenticator = authenticator;
        _pull.authenticator = authenticator;
        
        if(self._database == nil){
            Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot create database", dataToPrint: error!.localizedDescription);
        }
    }
    
    /*!
        Save attachement with the given data with the given name
    
        :param: data attachement as NSData
        :param: name Name of the attachement
    */
    
    func saveAttachement(data: NSData, name: String){
        
        var error: NSError?;
        
        var id = UserProfileManager.sharedInstance.userId;
        var doc: CBLDocument = _database.documentWithID(id)!;
        var unsavedRev: CBLUnsavedRevision? = doc.currentRevision?.createRevision();
        if(unsavedRev == nil){
            unsavedRev = doc.newRevision();
        }
        unsavedRev?.setAttachmentNamed(name, withContentType: "application/octet-stream", content: data)
        unsavedRev?.save(&error);
        
        if(error != nil){
            Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot save DB ", dataToPrint: error!.localizedDescription);
        }
    }
    
    /*!
    Get attachement with the given name
    
    :param: name Name of the attachement
    
    :returns: Returns the attachement as AnyObject?
    */
    
    func loadAttachementWithName(name: String)->AnyObject?{
        
        var error: NSError?;
        var id = UserProfileManager.sharedInstance.userId;
        var doc: CBLDocument = _database.documentWithID(id)!;
        var savedRev: CBLSavedRevision? = doc.currentRevision;
        if(savedRev == nil){
            return nil;
        }
        var attachement: CBLAttachment? = savedRev?.attachmentNamed(name);
        var newData = attachement?.content;
        if let data = newData{
            return NSKeyedUnarchiver.unarchiveObjectWithData(data);
        }
        return nil;
        
    }
    
    /*!
        Save data to couchDB document
        
        :param: dictionary Dictionary that has key/value pairs
    */
    func saveDataDictionary(dictionary : Dictionary<String, AnyObject>, var key: String?){
        
        //Retreive this document from DB, in case it's exists we update it
        var id = UserProfileManager.sharedInstance.userId;
        if(key == nil){
            key = id;
        }
        var setDocument: CBLDocument? = _database.documentWithID(key!);
        
        if(setDocument != nil){ //Document is existed
            
            var error: NSError?;
           // save the Document revision to the database
            
            setDocument!.update({ (var newRev) -> Bool in
                
                var mutabledic : NSMutableDictionary = NSMutableDictionary(capacity: 0);
                mutabledic.addEntriesFromDictionary(dictionary);
                newRev.properties = mutabledic;
                return true
            }, error: &error)
            

            if (error != nil){
                Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot save Data Dictionary", dataToPrint: error!.localizedDescription);
            }

            
        }
    }
    
    /*!
    Load data from couchDB document as a dictionary
    
    :param: id document id of the dictionary
    */
    func loadDataDictionary(id: String?) -> Dictionary<String, AnyObject>?{
        
        //Retreive this document from DB, in case it's exists we update it

        var getDocument: CBLDocument? = _database.documentWithID(id!);
        
        if(getDocument != nil){ //Document is existed
            return getDocument?.properties as? Dictionary<String, AnyObject>;
        }
        return nil;
    }
    
    /*!
    Save data to couchDB document
    
    :param: dictionary Dictionary that has key/value pairs
    */
    func saveDataKeyValue(value: AnyObject, key: String){
        
        //Retreive this document from DB, in case it's exists we update it
        var id = UserProfileManager.sharedInstance.userId;
        var setDocument: CBLDocument? = _database.documentWithID(id);
        
        if(setDocument != nil){ //Document is existed
            
            var error: NSError?;
            
            var dictionary : Dictionary<String, AnyObject> = setDocument?.properties as! Dictionary<String, AnyObject>;
            dictionary[key] = value;
            // save the Document revision to the database
            var newRev: CBLRevision? = setDocument?.putProperties(dictionary, error: &error);
            if (error != nil)
            {
                Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot save Data key/value", dataToPrint: error!.localizedDescription);
            }
        }
    }
    
    
    /*!
    Get data with key
    
    :param: key The keyfield of tyhe data to retrieve
    
    :returns: Data with this key
    */
    func getDocumentWithID(id: String) -> AnyObject?{
        
        var getDocument: CBLDocument? = _database.existingDocumentWithID(id);

        if(getDocument != nil){ //Document is existed
            let dictionary : Dictionary<String, AnyObject> = getDocument?.properties as! Dictionary<String, AnyObject>;
            return dictionary;
        }
     
        return nil;
    }
    
    
    /*!
        Compact db to get rid of unneeded files (temp files) and shrink db size
    */
    
    func compactDB(){
        
        var error: NSError?;
        self._database.compact(&error);
        if (error != nil)
        {
            Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot compact db", dataToPrint: error!.localizedDescription);
        }
        
        var couchDBPath = NSSearchPathForDirectoriesInDomains(.ApplicationSupportDirectory, .UserDomainMask, true)[0] as! String;
        
        couchDBPath += "/CouchbaseLite/";
        
        println(couchDBPath);
        var array: Array = NSFileManager.defaultManager().contentsOfDirectoryAtPath(couchDBPath, error: nil)!;
        
        for file in array{
            if(file.hasPrefix("(A Document Being Saved By Shoponga")){
                
                var fullPath = couchDBPath + (file as! String);
                
                if(NSFileManager.defaultManager().contentsOfDirectoryAtPath(fullPath, error: nil)?.count == 0){ //Directory is empty
                    NSFileManager.defaultManager().removeItemAtPath(fullPath, error: nil);
                }
                
            }
            
        }
        
    }
    
    
    /*!
        Synching db, syncing local with remote database
    */
    func syncdb(){
        
        self._database.setFilterNamed("UserData", asBlock: {
            (revision, params) -> Bool in
            
            return self.getArrayOfDocIDsToSync().contains(revision["_id"] as! String);  //Allow pushing user profile as a BLOB
        })
        
        
        _push.filter = "UserData";
        _pull.documentIDs = getArrayOfDocIDsToSync(); //Retrieve user data & shared documents
        
        println("Documents to pull: \(_pull.documentIDs)");
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "replicationProgress:", name: kCBLReplicationChangeNotification, object: _push)
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "replicationProgress:", name: kCBLReplicationChangeNotification, object: _pull)
        _push.start()
        _pull.start()
    }
    
    func replicationProgress(n: NSNotification) {
       
        var _syncError: NSError?
        
        if (_pull.status == CBLReplicationStatus.Active || _push.status == CBLReplicationStatus.Active) {
            // Sync is active -- aggregate the progress of both replications and compute a fraction:
            let completed = _pull.completedChangesCount + _push.completedChangesCount
            let total = _pull.changesCount + _push.changesCount
            // Update the progress bar, avoiding divide-by-zero exceptions:   
            
        }
        else if (_pull.status == CBLReplicationStatus.Stopped){
            println("Sync done")            
            
            UserProfileManager.sharedInstance.loadData();
            
            if (UserProfileManager.sharedInstance.listsArray == nil || UserProfileManager.sharedInstance.listsArray.count == 0){
                
                return;
            }
        }
        // Check for any change in error status and display new errors:
        let error = _pull.lastError ?? _push.lastError
        if (error != _syncError) {
            _syncError = error
            if error != nil {
                Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Cannot sync db", dataToPrint: error!.localizedDescription);
            }
        }
    }

    /*!
        Start pulling shoppinglist with the given ID from the remote server and integrat it locally
    
        :param: docID The Id of the document to retrieve
    */
    func pullDocumentWithIDRemotely(docID: String){
        
        _documentIdToPull = docID;
        
        _pull.documentIDs = [docID];
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "synchingShoppingListStatus:", name: kCBLReplicationChangeNotification, object: _pull)
        _pull.start()
    }
    
    func synchingShoppingListStatus(n: NSNotification){
        
        let listId = _documentIdToPull!;
        if(_pull.status == CBLReplicationStatus.Stopped){
            
            var dict : Dictionary<String, AnyObject> = self.getDocumentWithID(listId) as! Dictionary<String, AnyObject>;
            var x : ShoppingList = UserProfileManager.sharedInstance.convertDictionaryToShoppingList(dict);
            //Keep some data without syncing
            x.order = ShoppingListManager.sharedInstance.listsArray[listId]!.order;
            
            UserProfileManager.sharedInstance.listsArray[listId] = x;
            ShoppingListManager.sharedInstance.listsArray[listId] = x;            
        }
    }
    
    private func getArrayOfDocIDsToSync() -> [String]{
        
        var array : [String] = [UserProfileManager.sharedInstance.userId];
        
        if (UserProfileManager.sharedInstance.listsArray == nil){
            return array;
        }

        for list in UserProfileManager.sharedInstance.listsArray.values.array{  //Allow pushing shared and get remotely docuemnts.
            if(list.isShared == true){
                array.append(list.id);
            }
        }
        
        return array;
        
    }
}