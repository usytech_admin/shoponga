//
//  RetailerSearchFactory.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//



class RetailerSearchFactory {
    
    class func createRetailerObject(retailerObject : RetailerObject) -> Retailer{
        
        var retailer: Retailer;
        
        switch retailerObject.id{
        case "1":
            retailer = WalmartRetailer();
            break;
        case "2":
            retailer = BestbuyRetailer();
            break;
        default:
            retailer = WalmartRetailer();
            break;
            
        }
        
        return retailer;
        
    }
}
