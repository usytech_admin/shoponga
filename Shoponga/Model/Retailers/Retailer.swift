//
//  Retailer.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    The Abstract Retailer class, used mainly in the FactorY Esign pattern and it does include all
    common parameters and ethods for all retailers
*/


@objc protocol RetailerModelToViewDelegate {
    
    optional func autocompleteDataReturned (arr: Array<String>);
    optional func searchDataReturned (arr: Array<Item>);
    optional func detailsDataReturned (item: Item);
    
}


protocol RetailerBatchDelegate {
    
    func batchResultsReturned(arr: Array<Item>, retailerObject: RetailerObject);
    
}

protocol Retailer {        
    
    weak var retailerObject: RetailerObject! {get set}
    
    ///Delegate to reflect in view classes
    var delegate: RetailerModelToViewDelegate? { get set }
    
    ///Delegate to reflect the batch request data
    var batchDataDelegate: RetailerBatchDelegate? { get set }
    
    ///Delegate to reflect suggestions result
    var suggestionsDataDelegate: RetailerModelToViewDelegate? { get set }
    
    /*!
    Get retailer name.
    
    :return: Name of current retailer.
    */
    func getName()->String;
    

    /*!
    Start retrived suggested keywords (autocomplete).
    
    :param: query The item to start search for suggestions.
    */
    func autocompleteWithSuggestions(query:String);
    
    /*!
    Search for specified item.
    
    :param: query The item to start search for similar items.
    :param: pageNumber The index to start search, froom 0 to 10 pages
    */
    func searchForItem(query:String, pageNumber:Int);
    

    /*!
    Get item details.
    
    :param: item The item to get details.
    */
    func getItemDetails(item:Item);

    /*!
    Get items details (batch request, used for updates only).
    
    :param: itemsList List of itemsIDs
    */
    func updateItems(itemsIDsList: String);
    

    /*!
    Parse data from search operation.
    
    :param: dictionary The object returned as a Dictionary to be converted to Item object.
    */
    func parseSearchResults(dictionary:Dictionary<String, AnyObject>);
    
    
    /*!
    Parse data from item details response.
    
    :param: dictionary The object returned as a Dictionary to be converted to Item object.
    */
    func parseDetailsResult(dictionary:Dictionary<String, AnyObject>);


    /*!
    Parse batch data result
    
    :param: dictionary The object returned as a Dictionary to be converted to Item object.
    */
    func parseBatchResult(dictionary:Dictionary<String, AnyObject>);

    
    /*!
    Create Item object from the returned response.
    
    :param: dictionary The object returned as a Dictionary to be converted to Item object.
    */
    func createItemObject(dictionary:Dictionary<String, AnyObject>) -> Item;
    
}
