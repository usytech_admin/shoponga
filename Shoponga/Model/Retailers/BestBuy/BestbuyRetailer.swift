//
//  BestbuyRetailer.swift
//  Shoponga
//
//  Created by Elsammak on 7/1/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation
import Alamofire

class BestbuyRetailer : Retailer {
    
    
    var delegate : RetailerModelToViewDelegate?
    var batchDataDelegate : RetailerBatchDelegate?
    var suggestionsDataDelegate : RetailerModelToViewDelegate?
    
    var retailerObject : RetailerObject!;
    
// MARK: - Inits
    init (){
        
    }
    func getName() -> String {
        return retailerObject.name;
    }
    
    // MARK: - Start sending to server
    
    func autocompleteWithSuggestions(query:String){
        
        var autocompleteParams = [
            "method" : "completion",
            "search-alias" : "aps",
            "q" : query,
            "client" : "amazon-search-ui",
            "mkt" : "1",
        ]
        
        
        Alamofire.request(.GET, AMAZON_AUTOCOMPLETE_URL, parameters:autocompleteParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
                if let dataArray = data as? NSArray {
                    
                    var suggestedKeywords : Array<String> = dataArray[1] as! Array<String>;
                    if let delegate = self.suggestionsDataDelegate {
                        delegate.autocompleteDataReturned!(suggestedKeywords);
                    }
                    
                }
        }
    }
    
    
    func searchForItem(query:String, var pageNumber:Int){
        
        var pageNumberString : String = String(pageNumber);
        
        let url = NSURL(string: BESTBUY_SERVICE_URL+"products(search="+query+")")!
        let URLRequest = NSURLRequest(URL: url);
        
        var searchParams = [
            "apiKey" : BESTBUY_API_KEY,
            "page" : pageNumberString,
            "format" : "json",
            "show" : "productId,name,salePrice,image,type"
        ]
        
        Alamofire.request(.GET, URLRequest, parameters:searchParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
// Print URL requests
                if  PRINT_URL_REQUESTS == 1{
                    Logging.printLog(NSStringFromClass(self.dynamicType) , description: "BestBuy URL reqesut", dataToPrint: request.URL!);
                }
                
                //Start parsing in the background
                let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
                dispatch_async(backgroundQueue, {
                    if  PRINT_URL_RESPONSES == 1{
                        Logging.printLog(NSStringFromClass(self.dynamicType), description: "ItemsSearchResponse", dataToPrint: data!);
                    }
                    self.parseSearchResults(data! as! Dictionary<String, AnyObject>)
                })
        }
        
    }
    
    func getItemDetails(item:Item){
        
        
        let url = NSURL(string: BESTBUY_SERVICE_URL+"products(productId="+item.id+")")!
        var URLRequest = NSURLRequest(URL: url);
        
        let lookUpParams = [
            "apiKey" : BESTBUY_API_KEY,
            "format" : "json",
            "show" : "all"
        ]
        
        Alamofire.request(.GET, URLRequest, parameters:lookUpParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
                //Start parsing in the background
                let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
                dispatch_async(backgroundQueue, {
                    
                    //Print response
                    if  PRINT_URL_RESPONSES == 1{
                        Logging.printLog(NSStringFromClass(self.dynamicType), description: "ItemsDetailsResponse", dataToPrint: data!);
                    }
                    self.parseDetailsResult(data! as! Dictionary<String, AnyObject>)
                })
        }
        
        
    }
    
    
    func updateItems(itemsIDsList: String){
        
        let url = NSURL(string: BESTBUY_SERVICE_URL+"products(productId%20in%20("+itemsIDsList+"))")
        var URLRequest = NSURLRequest(URL: url!);
        
        let lookUpParams = [
            "apiKey" : BESTBUY_API_KEY,
            "format" : "json",
            "show" : "productId,name,salePrice,image,type"
        ]
        
        Alamofire.request(.GET, URLRequest, parameters:lookUpParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
                //Start parsing in the background
                let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
                dispatch_async(backgroundQueue, {
                    
                    //Print response
                    if  PRINT_URL_RESPONSES == 1{
                        Logging.printLog(NSStringFromClass(self.dynamicType), description: "ItemsBatchResponse", dataToPrint: data!);
                    }
                    self.parseBatchResult(data! as! Dictionary<String, AnyObject>)
                })
        }
        
        
        
    }
    
    // MARK: - Start parsing responses
    
    func parseSearchResults(dictionary:Dictionary<String, AnyObject>){
        
        var arr : [Item] = Array();
        
        if(dictionary.indexForKey("error") != nil){
            
            println("Error occured");
            //Reflect model to view class as UI must run in the mainThread
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let delegate = self.delegate {
                    delegate.searchDataReturned!(arr);
                }
            })
            
            return;
        }
        
        var arrayOfItems : Array<Dictionary<String, AnyObject>> = dictionary["products"]! as! Array<Dictionary<String, AnyObject>>;
        
        for itemObject in arrayOfItems{
            
            var item : Item = self.createItemObject(itemObject);
            arr.append(item);
        }
        
        //Reflect model to view class as UI must run in the mainThread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let delegate = self.delegate {
                delegate.searchDataReturned!(arr);
            }
        })
        
    }
    
    
    func parseDetailsResult(var dictionary:Dictionary<String, AnyObject>){
        
        var item : Item = Item();
        
        var arrayOfItems : Array<Dictionary<String, AnyObject>> = dictionary["products"]! as! Array<Dictionary<String, AnyObject>>;
        
        item = createItemObject(arrayOfItems[0]);
        
        //Reflect model to view class as UI must run in the mainThread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let delegate = self.delegate {
                delegate.detailsDataReturned!(item);
            }
        })
        
    }
    
    func parseBatchResult(dictionary:Dictionary<String, AnyObject>){
        
        var arr : [Item] = Array();
        
        if(dictionary.indexForKey("error") != nil){
            
            println("Error occured");
            //Reflect model to view class as UI must run in the mainThread
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let delegate = self.batchDataDelegate {
                    delegate.batchResultsReturned(arr, retailerObject: self.retailerObject);
                }
            })
            
            return;
        }
        
        
        var arrayOfItems : Array<Dictionary<String, AnyObject>> = dictionary["products"]! as! Array<Dictionary<String, AnyObject>>;
        
        for itemObject in arrayOfItems{
            
            var item : Item = self.createItemObject(itemObject);
            arr.append(item);
        }
        
        //Reflect model to view class as UI must run in the mainThread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let delegate = self.batchDataDelegate {
                delegate.batchResultsReturned(arr, retailerObject: self.retailerObject);
            }
        })
        
        
    }
    
    func createItemObject(dictionary: Dictionary<String, AnyObject>) -> Item {
        
        var item : Item = Item ();
        
        item.id = String(dictionary["productId"] as! Int);
        
        //Start reading images [optional in case of search]
        item.smallImage = dictionary["image"] as? String;
        
        item.name = dictionary["name"] as! String;
        item.category = dictionary["type"] as! String;
        
        if let desc = dictionary["longDescription"] as? String{
            item.desc = desc;
        }
        item.retailerId = retailerObject.id;
        
        item.price = dictionary["salePrice"] as? Float32
        
        
        return item;
        
    }
    
    
    
}
