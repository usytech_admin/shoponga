//
//  RetailerManager.swift
//  Shoponga
//
//  Created by Elsammak on 6/16/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

/*!
    This class is responsible for searching for items at retailers, it gets the search query and according to the Factory Design Pattern
    it searches for the item in the created Retailer
*/

import Foundation

class RetailerManager {
    
    static let sharedInstance = RetailerManager();
    
    var currentRetailer : Retailer!; ///This is the one to deal directly with customer
    var silentRetailer : Retailer?; ///This one to deal in the background while updating items
    private var _pageNumber : Int = 1;
    
    private init() {
        // Private initialization to ensure just one instance is created.
        
        setMainRetailer(RetailerObjectManager.sharedInstance.getRetailerWithID("1")!); //Set default to Walmart
    }
    
    /*!
    Set main retailer to start searching, default is Walmart
    
    :param: mainRetailer The retailer to start search in and get search data from
    */
    func setMainRetailer (mainRetailer: RetailerObject){
        currentRetailer = RetailerSearchFactory.createRetailerObject(mainRetailer);
        currentRetailer.retailerObject = mainRetailer;
    }

    /*!
    Set silent retailer to start updating
    
    :param: silentRetailer The retailer to start update data from
    */
    func setSilentRetailer (silentRetailer: RetailerObject){
        
        if (self.silentRetailer?.retailerObject.id == silentRetailer.id){
            return;
        }

        self.silentRetailer = RetailerSearchFactory.createRetailerObject(silentRetailer);
        self.silentRetailer?.retailerObject = silentRetailer;
    }
    
    /*!
    Get list of suggested keywords (autocomplete)
    
    :param: query The search query the user entered
    */
    func searchForSuggestedKeywords(query: String){
        
        currentRetailer.autocompleteWithSuggestions(query);
    }
    
    /*!
    Searching for this item
    
    :param: query The search query the user entered
    */
    func searchForItem(query: String){        
        currentRetailer.searchForItem(query, pageNumber: _pageNumber);
        _pageNumber++;
    }
    
    
    /*!
    Update items
    
    :param: itemsIDsList The list of items Ids to update
    */
    func updateItems(itemsIDsList: String){
        silentRetailer?.updateItems(itemsIDsList);
    }
    
    /*!
    Clear data and reset index
    */
    func endSearch(){        
        _pageNumber = 1;
    }
    
    
    /*!
    Get Items details
    
    :param: query The search query the user entered
    */
    func getItemDetails(item:Item){
        
        currentRetailer.getItemDetails(item);
        
    }

    
    
}