//
//  RetailerObjectManager.swift
//  Shoponga
//
//  Created by Elsammak on 6/30/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation
class RetailerObjectManager {
    
    static var sharedInstance = RetailerObjectManager(); //Singlton Instance
    var retailersList: [RetailerObject] = []; //Array of retailers
//    private var _savedRetailersList: [RetailerObject] = []; //Array of saved retailers
    
    private init() {
        // Private initialization to ensure just one instance is created.
    }
    
    
    /*!
    Initialize with list of retailers of SupportedRetailer.plist
    */
    func initListOfRetailers(){
        
        if(retailersList.count == 0){
            
            var myDict: NSDictionary?
            if let path = NSBundle.mainBundle().pathForResource("SupportedRetailers", ofType: "plist") {
                myDict = NSDictionary(contentsOfFile: path)
            }
            if let dict = myDict {
                
                var arrayOfRetailers : Dictionary<String, Dictionary<String, AnyObject>> = dict as! Dictionary<String, Dictionary<String, AnyObject>>;
                
                for retailer in arrayOfRetailers{
                    
                    var isSupported: Bool = retailer.1["IsSupported"] as! Bool;
                    if (!isSupported){
                        continue;
                    }
                    var id : String = retailer.1["Id"] as! String;
                    var name : String = retailer.1["Name"] as! String;
                    var logo: String = retailer.1["Logo"] as! String;
                    
                    var retailer: RetailerObject = RetailerObject();
                    retailer.id = id;
                    retailer.logo = logo;
                    retailer.name = name;
                    
                    retailersList.append(retailer);
                }
                
            }
            
        }
    }
    
    /*!
    Get RetailerObject with this ID
    
    :param: ID Id of this retailer
    
    :returns: RetailerObject with this id
    */
    func getRetailerWithID(id: String) -> RetailerObject?{
        
        if(retailersList.count == 0){
            initListOfRetailers();
        }
        
        for retailer in retailersList{
            if(retailer.id == id){
                return retailer;
            }
        }
        return nil;
    }
    
    
    /*!
    Get list of item for this retailer
    
    :param: retailerID Id of this retailer
    
    :returns: Items list bought from this retailer
    */
    func getListOfItemsForRetailer(retailerId: String) -> Dictionary<String, Item>?{
        
        if let list = getRetailerWithID(retailerId)?.itemsList as Dictionary<String, Item>?{
            
            return list;
        }
        return nil;
    }

    
    
    // MARK: Saving and Loading
    func save() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "RetailerObjectManager")
    }
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("RetailerObjectManager")
    }
    
    class func loadSaved() -> RetailerObjectManager {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("RetailerObjectManager") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as! RetailerObjectManager
        }
        return RetailerObjectManager()
    }

    
}