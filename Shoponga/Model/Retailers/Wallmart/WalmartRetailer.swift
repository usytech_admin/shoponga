//
//  WalmartRetailer.swift
//  Shoponga
//
//  Created by Elsammak on 6/21/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation
import Alamofire

class WalmartRetailer : Retailer {
    
    // MARK: - Inits
    
    var delegate : RetailerModelToViewDelegate?
    var batchDataDelegate : RetailerBatchDelegate?
    var suggestionsDataDelegate : RetailerModelToViewDelegate?
    
    var retailerObject : RetailerObject!;
    
    init (){
        
    }
    func getName() -> String {
        return retailerObject.name;
    }
    
    // MARK: - Start sending to server
    
    func autocompleteWithSuggestions(query:String){
        
        var autocompleteParams = [
            "method" : "completion",
            "search-alias" : "aps",
            "q" : query,
            "client" : "amazon-search-ui",
            "mkt" : "1",
        ]
        
        
        Alamofire.request(.GET, AMAZON_AUTOCOMPLETE_URL, parameters:autocompleteParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
                if let dataArray = data as? NSArray {
                    
                    var suggestedKeywords : Array<String> = dataArray[1] as! Array<String>;
                    if let suggestionsDataDelegate = self.suggestionsDataDelegate {
                        suggestionsDataDelegate.autocompleteDataReturned!(suggestedKeywords);
                    }
                    
                }
        }
    }
    
    
    func searchForItem(query:String, var pageNumber:Int){        
        
        var pageNumberString : String = String(pageNumber * 10);
        
        let URL = NSURL(string: WALMART_SERVICE_URL)!
        let URLRequest = NSURLRequest(URL: URL.URLByAppendingPathComponent("search"))
        
        var searchParams = [
            "apiKey" : WALMART_API_KEY,
            "query" : query,
            "start" : pageNumberString,
        ]
        
        Alamofire.request(.GET, URLRequest, parameters:searchParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
// Print URL requests
                if  PRINT_URL_REQUESTS == 1{
                    Logging.printLog(NSStringFromClass(self.dynamicType) , description: "Walmart URL reqesut", dataToPrint: request.URL!);
                }
                
                //Start parsing in the background
                let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
                dispatch_async(backgroundQueue, {
                    if  PRINT_URL_RESPONSES == 1{
                        Logging.printLog(NSStringFromClass(self.dynamicType), description: "ItemsSearchResponse", dataToPrint: data!);
                    }
                    self.parseSearchResults(data! as! Dictionary<String, AnyObject>)
                })
        }
        
    }
    
    func getItemDetails(item:Item){
        
        
        let URL = NSURL(string: WALMART_SERVICE_URL)!
        var URLRequest = NSURLRequest(URL: URL.URLByAppendingPathComponent("items/"+item.id))
        
        let lookUpParams = [
            "apiKey" : WALMART_API_KEY,
        ]
        
        Alamofire.request(.GET, URLRequest, parameters:lookUpParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
                //Start parsing in the background
                let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
                dispatch_async(backgroundQueue, {
                    
//Print response
                    if  PRINT_URL_RESPONSES == 1{
                        Logging.printLog(NSStringFromClass(self.dynamicType), description: "ItemsDetailsResponse", dataToPrint: data!);
                    }
                    self.parseDetailsResult(data! as! Dictionary<String, AnyObject>)
                })
        }

        
    }
    
    
    func updateItems(itemsIDsList: String){
        
        let url = NSURL(string: WALMART_SERVICE_URL+"items?ids="+itemsIDsList)!
        var URLRequest = NSURLRequest(URL: url);
        
        let lookUpParams = [
            "apiKey" : WALMART_API_KEY,
        ]
        
        Alamofire.request(.GET, URLRequest, parameters:lookUpParams, encoding: .URL) .responseJSON
            {
                (request, response, data, error) in
                
                //Start parsing in the background
                let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
                dispatch_async(backgroundQueue, {
                    
//Print response
                    if  PRINT_URL_RESPONSES == 1{
                        Logging.printLog(NSStringFromClass(self.dynamicType), description: "ItemsBatchResponse", dataToPrint: data!);
                    }
                    self.parseBatchResult(data! as! Dictionary<String, AnyObject>)
                })
        }
        

        
    }
    
// MARK: - Start parsing responses
    
    func parseSearchResults(dictionary:Dictionary<String, AnyObject>){
        
        var arr : [Item] = Array();

        if (dictionary.indexForKey("errors") != nil || dictionary["totalResults"] as? Int == 0){
            
            println("Error occured");
            //Reflect model to view class as UI must run in the mainThread
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let delegate = self.delegate {
                    delegate.searchDataReturned!(arr);
                }
            })
            
            return;
        }
        
        var arrayOfItems : Array<Dictionary<String, AnyObject>> = dictionary["items"]! as! Array<Dictionary<String, AnyObject>>;
        
        for itemObject in arrayOfItems{
            
            var item : Item = self.createItemObject(itemObject);
            arr.append(item);
        }
        
        //Reflect model to view class as UI must run in the mainThread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let delegate = self.delegate {
                delegate.searchDataReturned!(arr);
            }
        })
        
    }
    
    
    func parseDetailsResult(dictionary:Dictionary<String, AnyObject>){
        
        var item : Item = Item();
        
        item = createItemObject(dictionary);
        
        //Reflect model to view class as UI must run in the mainThread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let delegate = self.delegate {
                delegate.detailsDataReturned!(item);
            }
        })
        
    }
    
    func parseBatchResult(dictionary:Dictionary<String, AnyObject>){
        
        var arr : [Item] = Array();
        
        if(dictionary.indexForKey("errors") != nil){
            
            println("Error occured");
            //Reflect model to view class as UI must run in the mainThread
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let delegate = self.batchDataDelegate {
                    delegate.batchResultsReturned(arr, retailerObject: self.retailerObject);
                }
            })
            
            return;
        }

        
        var arrayOfItems : Array<Dictionary<String, AnyObject>> = dictionary["items"]! as! Array<Dictionary<String, AnyObject>>;
        
        for itemObject in arrayOfItems{
            
            var item : Item = self.createItemObject(itemObject);
            arr.append(item);
        }
        
        //Reflect model to view class as UI must run in the mainThread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if let delegate = self.batchDataDelegate {
                delegate.batchResultsReturned(arr, retailerObject: self.retailerObject);
            }
        })

        
    }
    
    func createItemObject(dictionary: Dictionary<String, AnyObject>) -> Item {
        
        var item : Item = Item ();
        
        item.id = String(dictionary["itemId"] as! Int);
        
        //Start reading images [optional in case of search]
        item.smallImage = dictionary["thumbnailImage"] as? String;
        item.largeImage = dictionary["largeImage"] as? String;
        
        item.name = dictionary["name"] as! String;
        item.category = dictionary["categoryPath"] as! String;
        
        item.desc = dictionary["longDescription"] as? String;
        
        item.retailerId = retailerObject.id;
        
        item.price = dictionary["salePrice"] as? Float32

        
        return item;
        
    }
    
    
    
}