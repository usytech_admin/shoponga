//
//  RetailerObject.swift
//  Shoponga
//
//  Created by Elsammak on 6/29/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import Foundation

class RetailerObject {
    
    
    var id: String!;
    var name: String!;
    var logo: String!;
    var itemsList: Dictionary<String, Item> = Dictionary<String, Item>();
    
    init(){
        
    }
    
    func addItem(item: Item){
        
        itemsList[item.id] = item;
    }
    func removeItem(item: Item){
        
        itemsList.removeValueForKey(item.id);
    }

}