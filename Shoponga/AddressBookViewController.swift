//
//  AddressBookViewController.swift
//  Shoponga
//
//  Created by Elsammak on 7/12/15.
//  Copyright (c) 2015 USYTech. All rights reserved.
//

import UIKit
import AddressBook
import MessageUI


protocol addressBookProtocol{
    
    func showOrHideAddressBook()
    
}

class AddressBookViewController: UIViewController , MFMessageComposeViewControllerDelegate, UISearchBarDelegate , MFMailComposeViewControllerDelegate , UIScrollViewDelegate, AddressBookCellProtocol{
    
    var SMS_MESSAGE = NSLocalizedString("SMS Message", comment: "")
    var EMAIL_MESSAGE = NSLocalizedString("Email Body", comment: "")
    let EMAIL_SUBJECT = NSLocalizedString("Email Subject", comment: "")
    var listId = "";
    
    @IBOutlet var closeAddressBook : UIButton!
    @IBOutlet var sendButton : UIButton!
    @IBOutlet var contactsTableView : UITableView!
    @IBOutlet var searchBar : UISearchBar!
    
    var delegate: addressBookProtocol! = nil
    var addressBook : ABAddressBookRef!
    var contactsArray: NSMutableArray = NSMutableArray()
    var contactsFilteredArray: NSMutableArray = NSMutableArray()
    var emailsArray: NSMutableArray = NSMutableArray()
    var mobilesArray: NSMutableArray = NSMutableArray()
    var mc : MFMailComposeViewController = MFMailComposeViewController();
    var isFiltering = false
    
    private var numberOfSelectedEmails = 0;
    private var numberOfSelectedSMS = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        contactsTableView.reloadData()
    }
    
    func startOpenContactsBook(){
        
        self.getContactNames();
        
        //Testing line
        listId = "909B52D7-1772-4B4D-B2E9-C0EC884FE3A9399031:03:32.390";
        //
        var invitationLink: String = SHOPONGA_URLSCHEME + "source=sharing" + "&shareListID="+listId;
        EMAIL_MESSAGE = EMAIL_MESSAGE.stringByReplacingOccurrencesOfString("%@", withString: "<a href=\(invitationLink)>Click here</a>")
        SMS_MESSAGE = SMS_MESSAGE.stringByReplacingOccurrencesOfString("%@", withString: invitationLink)
    }
    
    // MARK: UIsearchBar Methods
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (count(searchText) == 0){
            
            isFiltering = false
            
        }
        else{
            
            isFiltering = true
            contactsFilteredArray.removeAllObjects()
            
            for contactObject in contactsArray{
                
                if let contact = contactObject as? ABContact{
                    
                    
                    var nameRange: NSRange = (contact.name as NSString).rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
                    
                    if(nameRange.location != NSNotFound)
                    {
                        contactsFilteredArray.addObject(contact);
                    }
                    
                }
                
            }
        }
        
        contactsTableView.reloadData()
        
    }
    
    
    
    
    // MARK: UITableView Methods
    
    func tableView(tableView: UITableView,
        heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
            
            var contact: ABContact!
            
            if(isFiltering){
                contact = contactsFilteredArray.objectAtIndex(indexPath.row) as! ABContact
            }
            else{
                contact = contactsArray.objectAtIndex(indexPath.row) as! ABContact
            }
            
            
            if(contact.isExpanded){
                return contact.heightOfCell
            }
            else{
                return 30;
            }
            
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!{
        
        
        let identifier = "Cell"
        
        var cell: AddressBookEntryCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? AddressBookEntryCell
        
        if cell == nil {
            tableView.registerNib(UINib(nibName: "AddressBookEntryCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCellWithIdentifier(identifier) as? AddressBookEntryCell
        }
        
        cell.delegate = self
        var contact: ABContact!
        
        if(isFiltering){
            contact = contactsFilteredArray.objectAtIndex(indexPath.row) as! ABContact
        }
        else{
            contact = contactsArray.objectAtIndex(indexPath.row) as! ABContact
        }
        
        cell.contact = contact
        
        cell.adjustCellView()
        
        return cell;
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int{
        
        if(isFiltering){
            
            return contactsFilteredArray.count;
        }
        else{
            return contactsArray.count;
        }
        
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!){
        
        var contact: ABContact = contactsArray.objectAtIndex(indexPath.row) as! ABContact
        
        if(isFiltering){
            contact = contactsFilteredArray.objectAtIndex(indexPath.row) as! ABContact
        }
        
        
        var arr = NSMutableArray()
        
        arr.addObject(indexPath)
        
        
        //Select first entry by default
        var cell: AddressBookEntryCell = tableView.cellForRowAtIndexPath(indexPath) as! AddressBookEntryCell;
        
        if(!contact.isThereAnyThingSelected() && !contact.isExpanded){
            
            if(cell.isMobileButtonSet){
                cell.mobileButtonPressed();
            }
            else if(cell.isIphoneButtonSet){
                cell.iPhoneButtonPressed();
            }
            else if(cell.isHomeEmailButtonSet){
                cell.homeEmailButtonPressed();
            }
            else if(cell.isWorkEmailButtonSet){
                cell.workEmailButtonPressed();
            }
            
        }
        
        contact.isExpanded = !contact.isExpanded
        
        if(isFiltering){
            isFiltering = false
            contactsTableView.reloadData()
            searchBar.text = ""
            searchBar.resignFirstResponder()
            
            var scrollToIndexPath = NSIndexPath(forRow: contact.index, inSection: 0);
            tableView.scrollToRowAtIndexPath(scrollToIndexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: false)
        }
        else{
            tableView.reloadRowsAtIndexPaths(arr as [AnyObject], withRowAnimation: UITableViewRowAnimation.None)
        }
        
        
        
        
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        
        if(searchBar.text == ""){
            searchBar.text = ""
            searchBar.resignFirstResponder()
        }
    }
    
    // MARK: Cell delegate methods
    func emailSelected(isSelected: Bool){
        
        if(isSelected){
            
            numberOfSelectedEmails++;
            shouldChangeSendInvitaionsButton(true);
        }
        else{
            numberOfSelectedEmails--;
            if(numberOfSelectedEmails == 0 && numberOfSelectedSMS == 0){
                shouldChangeSendInvitaionsButton(false);
            }
        }
        
    }
    func mobileSelected(isSelected: Bool){
        
        if(isSelected){
            
            numberOfSelectedSMS++;
            shouldChangeSendInvitaionsButton(true);
        }
        else{
            numberOfSelectedSMS--;
            if(numberOfSelectedEmails == 0 && numberOfSelectedSMS == 0){
                shouldChangeSendInvitaionsButton(false);
            }
        }
    }
    func shouldChangeSendInvitaionsButton(isSomethingSelected: Bool){
        
        if(isSomethingSelected){
            
            sendButton.alpha = 1;
        }
        else{
            sendButton.alpha = 0;
        }
        
    }
    // MARK: IBAction methods
    @IBAction func closeAddressBookButtonPressed(){
        
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        contactsArray.removeAllObjects()
        contactsFilteredArray.removeAllObjects()
        shouldChangeSendInvitaionsButton(false);
        delegate.showOrHideAddressBook()
        
    }
    
    @IBAction func sendButtonPressed(){
        
        emailsArray.removeAllObjects()
        mobilesArray.removeAllObjects()
        sendEmailComposer()
    }
    // MARK: Sending Methods
    func sendEmailComposer(){
        
        for contactObject in contactsArray{
            
            if let contact = contactObject as? ABContact{
                
                if contact.ishomeEmailSelected{
                    
                    emailsArray.addObject(contact.homeEmail!)
                }
                if contact.isWorkEmailSelected{
                    
                    emailsArray.addObject(contact.workEmail!)
                }
                
            }
            
        }
        
        if(emailsArray.count == 0){
            
            sendSMSComposer()
            return;
        }
        
        //Show Email composer
        
        var emailTitle = EMAIL_SUBJECT
        var messageBody = EMAIL_MESSAGE
        var toRecipents = emailsArray        
        
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: true)
        mc.setToRecipients(toRecipents as [AnyObject])
        
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mc, animated: true, completion: nil)
        } else {
            
            let sendMailErrorAlert = UIAlertView(title: NSLocalizedString("Could Not Send Email-Title", comment: ""), message: NSLocalizedString("Could Not Send Email-Message", comment: ""), delegate: self, cancelButtonTitle: "OK")
            sendMailErrorAlert.show()
        }
        
    }
    
    func sendSMSComposer(){
        
        
        for contactObject in contactsArray{
            
            if let contact = contactObject as? ABContact{
                
                if contact.isMobileSelected{
                    
                    mobilesArray.addObject(contact.mobileNumber!)
                }
                if contact.isIphoneSelected{
                    
                    mobilesArray.addObject(contact.iPhoneNumber!)
                }
                
            }
            
        }
        
        if(mobilesArray.count == 0){
            
            if(emailsArray.count == 0){
                let sendMailErrorAlert = UIAlertView(title: NSLocalizedString("Could Not Send Error-Title", comment: ""), message:NSLocalizedString("Could Not Send Error-Message", comment: ""), delegate: self, cancelButtonTitle: "OK")
                sendMailErrorAlert.show()
            }
            
            return;
        }
        
        //Show sms composer
        
        let messageController = MFMessageComposeViewController()
        
        if (!MFMessageComposeViewController.canSendText()) {
            var warningAlert : UIAlertView = UIAlertView.alloc();
            warningAlert.title = NSLocalizedString("Could Not Send SMS-Title", comment: "")
            warningAlert.message = NSLocalizedString("Could Not Send SMS-Message", comment: "")
            warningAlert.delegate = nil;
            warningAlert.show();
            return;
        }
        
        messageController.messageComposeDelegate = self
        messageController.recipients = mobilesArray as [AnyObject]
        messageController.body = SMS_MESSAGE
        
        self.presentViewController(messageController, animated: true, completion: nil);
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        
        if (result.value == MessageComposeResultCancelled.value) {
            //            NSLog("Message was cancelled.");
        }
        else if (result.value == MessageComposeResultFailed.value) {
            var warningAlert : UIAlertView = UIAlertView.alloc();
            warningAlert.title = NSLocalizedString("Could Not Send SMS-Title", comment: "")
            warningAlert.message = NSLocalizedString("Could Not Send SMS-Message", comment: "")
            warningAlert.delegate = nil;
            warningAlert.show();
            //            NSLog("Message failed.");
        } else {
            //            NSLog("Message was sent.");
            
//            println("Number of invitations sent \(mobilesArray.count) and numbers of sms sent \(mobilesArray.count)")
        }
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError) {
        switch result.value {
            case MFMailComposeResultCancelled.value:
                println("Mail cancelled")
            //        case MFMailComposeResultSaved.value:
            //            NSLog("Mail saved")
            case MFMailComposeResultSent.value:
                println("Number of invitations sent \(emailsArray.count) and numbers of sms sent \(mobilesArray.count)")
            default:
            break
        }
        self.dismissViewControllerAnimated(false, completion: nil)
        
        sendSMSComposer()
    }
    
    // MARK: AddressBook Methods
    func determineStatus() -> Bool {
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
        case .Authorized:
            return self.createAddressBook()
        case .NotDetermined:
            var ok = false
            ABAddressBookRequestAccessWithCompletion(nil) {
                (granted:Bool, err:CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if granted {
                        ok = self.createAddressBook()
                        self.getContactNames()
                    }
                }
            }
            if ok == true {
                return true
            }
            self.addressBook = nil
            return false
        case .Restricted:
            self.addressBook = nil
            return false
        case .Denied:
            self.addressBook = nil
            return false
        }
    }
    
    
    func createAddressBook() -> Bool {
        if self.addressBook != nil {
            return true
        }
        var err : Unmanaged<CFError>? = nil
        let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
        if adbk == nil {
            //            println(err)
            self.addressBook = nil
            return false
        }
        self.addressBook = adbk
        
        return true
    }
    
    func getContactNames() {
        if !self.determineStatus() {
            var warningAlert : UIAlertView = UIAlertView.alloc();
            warningAlert.title = NSLocalizedString("Could Not Open contacts-Title", comment: "")
            warningAlert.message = NSLocalizedString("Could Not Open contacts-Message", comment: "")
            warningAlert.delegate = nil;
            warningAlert.show();
            return
        }
        
        contactsArray.removeAllObjects()
        
        let source = ABAddressBookCopyDefaultSource(addressBook);
        
        let people = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, nil, ABPersonSortOrdering()).takeRetainedValue() as NSArray as! [ABRecord]
        
        var index = 0;
        for person in people {
            
            if Int(ABRecordGetRecordType(person)) == kABPersonType
            {
                let personObject = ABRecordCopyCompositeName(person);
                if personObject == nil{
                    continue;
                }

                var name: String = personObject.takeUnretainedValue() as String

                var mobile: CFString?
                var iPhone: CFString?
                var workEmail: CFString?
                var homeEmail: CFString?
                
                var emailProperty: ABMultiValueRef? = ABRecordCopyValue(person, kABPersonEmailProperty).takeUnretainedValue() as ABMultiValueRef;
                
                for var emailIndex = 0; emailIndex<ABMultiValueGetCount(emailProperty); emailIndex++
                {
                    
                    var label = ABMultiValueCopyLabelAtIndex(emailProperty,emailIndex)?.takeUnretainedValue() as CFString?
                    
                    if let labelValue = label{
                        //                        println("Label has value ");
                    }
                    else{
                        label = kABWorkLabel;
                    }
                    
                    
                    let compareResultOfWork = CFStringCompare(label, kABWorkLabel, CFStringCompareFlags.CompareCaseInsensitive)
                    
                    let compareResultOfHome = CFStringCompare(label, kABHomeLabel, CFStringCompareFlags.CompareCaseInsensitive)
                    
                    if  compareResultOfWork == CFComparisonResult.CompareEqualTo
                    {
                        workEmail = (ABMultiValueCopyValueAtIndex (emailProperty,emailIndex).takeUnretainedValue() as! CFString);
                        
                    }
                    else if compareResultOfHome == CFComparisonResult.CompareEqualTo
                    {
                        homeEmail = (ABMultiValueCopyValueAtIndex (emailProperty,emailIndex).takeUnretainedValue() as! CFString);
                    }
                    
                }
                
                var phoneProperty: ABMultiValueRef = ABRecordCopyValue(person,kABPersonPhoneProperty).takeUnretainedValue() as ABMultiValueRef;
                
                for var phoneNumberIndex = 0; phoneNumberIndex<ABMultiValueGetCount(phoneProperty); phoneNumberIndex++
                {
                    
                    var label = ABMultiValueCopyLabelAtIndex(phoneProperty,phoneNumberIndex)?.takeUnretainedValue() as CFString?;
                    
                    if let labelValue = label{
                        //                        println("Label has value ");
                    }
                    else{
                        label = kABPersonPhoneMobileLabel;
                    }
                    
                    if label! == kABPersonPhoneMobileLabel
                    {
                        mobile = (ABMultiValueCopyValueAtIndex (phoneProperty,phoneNumberIndex).takeUnretainedValue() as! CFString);
                        
                    }
                    else if label! == kABPersonPhoneIPhoneLabel
                    {
                        iPhone = (ABMultiValueCopyValueAtIndex (phoneProperty,phoneNumberIndex).takeUnretainedValue() as! CFString);
                    }
                }
                
                if  (mobile != nil || iPhone != nil || workEmail != nil || homeEmail != nil ){
                    
                    var contact: ABContact = ABContact()
                    contact.name = name;
                    contact.workEmail = workEmail as? String;
                    contact.homeEmail = homeEmail as? String;
                    contact.mobileNumber = mobile as? String;
                    contact.iPhoneNumber = iPhone as? String;
                    contact.index = index++;
                    contactsArray.addObject(contact)
                }
            
                
            }
        }
        
    }
    
    deinit{
        
    }
}

