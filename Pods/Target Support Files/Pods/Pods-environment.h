
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AHKActionSheet
#define COCOAPODS_POD_AVAILABLE_AHKActionSheet
#define COCOAPODS_VERSION_MAJOR_AHKActionSheet 0
#define COCOAPODS_VERSION_MINOR_AHKActionSheet 1
#define COCOAPODS_VERSION_PATCH_AHKActionSheet 3

// Alamofire
#define COCOAPODS_POD_AVAILABLE_Alamofire
#define COCOAPODS_VERSION_MAJOR_Alamofire 1
#define COCOAPODS_VERSION_MINOR_Alamofire 2
#define COCOAPODS_VERSION_PATCH_Alamofire 3

// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 2
#define COCOAPODS_VERSION_PATCH_Bolts 0

// Bolts/AppLinks
#define COCOAPODS_POD_AVAILABLE_Bolts_AppLinks
#define COCOAPODS_VERSION_MAJOR_Bolts_AppLinks 1
#define COCOAPODS_VERSION_MINOR_Bolts_AppLinks 2
#define COCOAPODS_VERSION_PATCH_Bolts_AppLinks 0

// Bolts/Tasks
#define COCOAPODS_POD_AVAILABLE_Bolts_Tasks
#define COCOAPODS_VERSION_MAJOR_Bolts_Tasks 1
#define COCOAPODS_VERSION_MINOR_Bolts_Tasks 2
#define COCOAPODS_VERSION_PATCH_Bolts_Tasks 0

// EAColourfulProgressView
#define COCOAPODS_POD_AVAILABLE_EAColourfulProgressView
#define COCOAPODS_VERSION_MAJOR_EAColourfulProgressView 0
#define COCOAPODS_VERSION_MINOR_EAColourfulProgressView 1
#define COCOAPODS_VERSION_PATCH_EAColourfulProgressView 0

// FBSDKCoreKit
#define COCOAPODS_POD_AVAILABLE_FBSDKCoreKit
#define COCOAPODS_VERSION_MAJOR_FBSDKCoreKit 4
#define COCOAPODS_VERSION_MINOR_FBSDKCoreKit 4
#define COCOAPODS_VERSION_PATCH_FBSDKCoreKit 0

// FBSDKCoreKit/arc
#define COCOAPODS_POD_AVAILABLE_FBSDKCoreKit_arc
#define COCOAPODS_VERSION_MAJOR_FBSDKCoreKit_arc 4
#define COCOAPODS_VERSION_MINOR_FBSDKCoreKit_arc 4
#define COCOAPODS_VERSION_PATCH_FBSDKCoreKit_arc 0

// FBSDKCoreKit/no-arc
#define COCOAPODS_POD_AVAILABLE_FBSDKCoreKit_no_arc
#define COCOAPODS_VERSION_MAJOR_FBSDKCoreKit_no_arc 4
#define COCOAPODS_VERSION_MINOR_FBSDKCoreKit_no_arc 4
#define COCOAPODS_VERSION_PATCH_FBSDKCoreKit_no_arc 0

// FBSDKLoginKit
#define COCOAPODS_POD_AVAILABLE_FBSDKLoginKit
#define COCOAPODS_VERSION_MAJOR_FBSDKLoginKit 4
#define COCOAPODS_VERSION_MINOR_FBSDKLoginKit 4
#define COCOAPODS_VERSION_PATCH_FBSDKLoginKit 0

// FBSDKShareKit
#define COCOAPODS_POD_AVAILABLE_FBSDKShareKit
#define COCOAPODS_VERSION_MAJOR_FBSDKShareKit 4
#define COCOAPODS_VERSION_MINOR_FBSDKShareKit 4
#define COCOAPODS_VERSION_PATCH_FBSDKShareKit 0

// Fabric
#define COCOAPODS_POD_AVAILABLE_Fabric
#define COCOAPODS_VERSION_MAJOR_Fabric 1
#define COCOAPODS_VERSION_MINOR_Fabric 2
#define COCOAPODS_VERSION_PATCH_Fabric 8

// Fabric/Base
#define COCOAPODS_POD_AVAILABLE_Fabric_Base
#define COCOAPODS_VERSION_MAJOR_Fabric_Base 1
#define COCOAPODS_VERSION_MINOR_Fabric_Base 2
#define COCOAPODS_VERSION_PATCH_Fabric_Base 8

// FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK 6
#define COCOAPODS_VERSION_MINOR_FlurrySDK 5
#define COCOAPODS_VERSION_PATCH_FlurrySDK 0

// FlurrySDK/FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK_FlurrySDK 6
#define COCOAPODS_VERSION_MINOR_FlurrySDK_FlurrySDK 5
#define COCOAPODS_VERSION_PATCH_FlurrySDK_FlurrySDK 0

// Instabug
#define COCOAPODS_POD_AVAILABLE_Instabug
#define COCOAPODS_VERSION_MAJOR_Instabug 4
#define COCOAPODS_VERSION_MINOR_Instabug 1
#define COCOAPODS_VERSION_PATCH_Instabug 4

// LMDropdownView
#define COCOAPODS_POD_AVAILABLE_LMDropdownView
#define COCOAPODS_VERSION_MAJOR_LMDropdownView 1
#define COCOAPODS_VERSION_MINOR_LMDropdownView 0
#define COCOAPODS_VERSION_PATCH_LMDropdownView 0

// MCSwipeTableViewCell
#define COCOAPODS_POD_AVAILABLE_MCSwipeTableViewCell
#define COCOAPODS_VERSION_MAJOR_MCSwipeTableViewCell 2
#define COCOAPODS_VERSION_MINOR_MCSwipeTableViewCell 1
#define COCOAPODS_VERSION_PATCH_MCSwipeTableViewCell 4

// PopoverView
#define COCOAPODS_POD_AVAILABLE_PopoverView
#define COCOAPODS_VERSION_MAJOR_PopoverView 0
#define COCOAPODS_VERSION_MINOR_PopoverView 0
#define COCOAPODS_VERSION_PATCH_PopoverView 2

// SHXMLParser
#define COCOAPODS_POD_AVAILABLE_SHXMLParser
#define COCOAPODS_VERSION_MAJOR_SHXMLParser 1
#define COCOAPODS_VERSION_MINOR_SHXMLParser 1
#define COCOAPODS_VERSION_PATCH_SHXMLParser 0

// SWTableViewCell
#define COCOAPODS_POD_AVAILABLE_SWTableViewCell
#define COCOAPODS_VERSION_MAJOR_SWTableViewCell 0
#define COCOAPODS_VERSION_MINOR_SWTableViewCell 3
#define COCOAPODS_VERSION_PATCH_SWTableViewCell 7

// TwitterCore
#define COCOAPODS_POD_AVAILABLE_TwitterCore
#define COCOAPODS_VERSION_MAJOR_TwitterCore 1
#define COCOAPODS_VERSION_MINOR_TwitterCore 9
#define COCOAPODS_VERSION_PATCH_TwitterCore 0

// TwitterKit
#define COCOAPODS_POD_AVAILABLE_TwitterKit
#define COCOAPODS_VERSION_MAJOR_TwitterKit 1
#define COCOAPODS_VERSION_MINOR_TwitterKit 9
#define COCOAPODS_VERSION_PATCH_TwitterKit 0

